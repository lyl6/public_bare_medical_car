/*
 * 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
 * 开发板官网：www.lckfb.com
 * 技术支持常驻论坛，任何技术问题欢迎随时交流学习
 * 立创论坛：club.szlcsc.com
 * 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
 * 不靠卖板赚钱，以培养中国工程师为己任
 * Change Logs:
 * Date           Author       Notes
 * 2023-07-24     LCKFB-yzh    first version
 */
#include "board.h"
#include "bsp_encoder.h"
#include "positional_pid.h"
#include "bsp_motor.h"

typedef enum
{
    APP_PID_INIT = 0,
    APP_PID_RUN,
    APP_PID_STOP
} app_pid_status_t;

static app_pid_status_t app_pid_status = APP_PID_INIT;

static positional_pid_params_t motor1_speed_pid;
static positional_pid_params_t motor2_speed_pid;

static float motor1_speed_target = 0.0f;
static float motor2_speed_target = 0.0f;

void app_pid_run(void)
{
    static int16_t motor1_pwm_value = 0;
    static int16_t motor2_pwm_value = 0;
    switch (app_pid_status)
    {
    case APP_PID_INIT:
        // 速度环配置
        //  motor1 速度环 pid 参数初始化
        positional_pid_init(&motor1_speed_pid, 63, 25, 0, 0.01, 1000, -1000);
        // 修改motor1 速度环 kp ki kd
        motor1_speed_pid.positional_pid_set_value(&motor1_speed_pid, 63, 25,
                                                  0,8.0f,800,-800);

        // motor2 速度环 pid 参数初始化
        positional_pid_init(&motor2_speed_pid, 63, 25, 0, 0.01, 1000, -1000);
        // 修改motor2 速度环 kp ki kd
        motor1_speed_pid.positional_pid_set_value(&motor2_speed_pid, 63, 25,
                                                  0,8.0f,800,-800);
        app_pid_status++;
        break;
    case APP_PID_RUN:
        // 计算电机1的PWM输出值
        motor1_pwm_value = (int16_t)positional_pid_compute(
            &motor1_speed_pid, motor1_speed_target, get_m1_speed());
        // 应用电机1的PWM值
        motor1_pwm_value_set(motor1_pwm_value);

        // 计算电机2的pwm输出值
        motor2_pwm_value = (int16_t)positional_pid_compute(
            &motor2_speed_pid, motor2_speed_target, get_m2_speed());
        // 应用电机2的PWM值
        motor2_pwm_value_set(motor2_pwm_value);

#if DEBUG_PID_MOTOR1_SPEED == 1
        printf("pid_speed:%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d\n",
               motor1_speed_target, get_m1_speed(), motor1_speed_pid.p_out,
               motor1_speed_pid.i_out, motor1_speed_pid.d_out,
               motor1_speed_pid.error, motor1_pwm_value);
#endif
#if DEBUG_PID_MOTOR2_SPEED == 1
        printf("pid_speed:%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d\n",
               motor2_speed_target, get_m2_speed(), motor2_speed_pid.p_out,
               motor2_speed_pid.i_out, motor2_speed_pid.d_out,
               motor2_speed_pid.error, motor1_pwm_value);
#endif
        break;
    case APP_PID_STOP:
        break;
    default:
        break;
    }
}
