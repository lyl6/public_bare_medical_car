#include "board.h"
#include "ringbuffer.h"

extern void app_pid_run(void);

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    board_init();
    bsp_motor_init();
    bsp_encoder_init();
    bsp_led_init();
    bsp_uart_init();
    buzzer_init();
	
    // 设置SOFT_TIMER_0的超时时间为50ms，重复定时器，给LED闪烁使用
    soft_timer_repeat_init(SOFT_TIMER_0, 50);
    // 设置SOFT_TIMER_1的超时时间为200ms，重复定时器，给PID计算使用
    soft_timer_repeat_init(SOFT_TIMER_1, 20);
    // 设置SOFT_TIMER_3的超时时间为300ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_2, 300);
    // 设置SOFT_TIMER_4的超时时间为4000ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_3, 400);

    while (1)
    {
        // 让梁山派上的四个led灯轮流来回亮
        if (soft_timer_is_timeout(SOFT_TIMER_0))
        {
            bsp_led_left_right_move();
        }
        if (soft_timer_is_timeout(SOFT_TIMER_1))
        {
            app_pid_run();
        }

    }
}
