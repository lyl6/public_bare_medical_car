#include "board.h"
#include "ringbuffer.h"


/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    board_init();
    bsp_motor_init();
    bsp_encoder_init();
    bsp_led_init();
    bsp_uart_init();
    buzzer_init();

    // 初始化四个软件定时器
    // 设置SOFT_TIMER_0的超时时间为100ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_0, 50);
    // 设置SOFT_TIMER_1的超时时间为200ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_1, 10);
    // 设置SOFT_TIMER_3的超时时间为300ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_2, 300);
    // 设置SOFT_TIMER_4的超时时间为4000ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_3, 400);

    motor1_pwm_value_set(200);
    motor2_pwm_value_set(200);

    while (1)
    {
        // 让梁山派上的四个led灯轮流来回亮
        if (soft_timer_is_timeout(SOFT_TIMER_0))
        {
            bsp_led_left_right_move();
        }
        if (soft_timer_is_timeout(SOFT_TIMER_1))
        {
            printf("m1,m2 speed:%.3f,%.3f\n", get_m1_speed(), get_m2_speed());
        }

    }
}
