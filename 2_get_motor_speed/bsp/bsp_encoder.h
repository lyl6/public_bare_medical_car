/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
 */
#ifndef __BSP_ENCODER_H__
#define __BSP_ENCODER_H__

#include <stdio.h>
/* defined the M1 Encoder timer -*/
#define M1_TIMER                       TIMER2
#define M1_TIMER_CLK                   RCU_TIMER2
#define M1_TIMER_IRQn                  TIMER2_IRQn
#define M1_TIMER_IRQHANDLER            TIMER2_IRQHandler

/* defined the M1 Encoder A Phase pin: PB4 -*/
#define M1_A_TIMER_CH                  TIMER_CH_0
#define M1_A_GPIO_CLK                  RCU_GPIOB
#define M1_A_GPIO_PORT                 GPIOB
#define M1_A_GPIO_ALT_FUNC             GPIO_AF_2
#define M1_A_GPIO_PUPD                 GPIO_PUPD_NONE
#define M1_A_GPIO_PIN                  GPIO_PIN_4
/* defined the M1 Encoder B Phase pin: PB5 */
#define M1_B_TIMER_CH                  TIMER_CH_1
#define M1_B_GPIO_CLK                  RCU_GPIOB
#define M1_B_GPIO_PORT                 GPIOB
#define M1_B_GPIO_ALT_FUNC             GPIO_AF_2
#define M1_B_GPIO_PUPD                 GPIO_PUPD_NONE
#define M1_B_GPIO_PIN                  GPIO_PIN_5

/* defined the M2 Encoder timer -*/
#define M2_TIMER                       TIMER1
#define M2_TIMER_CLK                   RCU_TIMER1
#define M2_TIMER_IRQn                  TIMER1_IRQn
#define M2_TIMER_IRQHANDLER            TIMER1_IRQHandler
/* defined the M2 Encoder A Phase pin: PB8 -*/
#define M2_A_TIMER_CH                  TIMER_CH_0
#define M2_A_GPIO_CLK                  RCU_GPIOB
#define M2_A_GPIO_PORT                 GPIOB
#define M2_A_GPIO_ALT_FUNC             GPIO_AF_2
#define M2_A_GPIO_PUPD                 GPIO_PUPD_NONE
#define M2_A_GPIO_PIN                  GPIO_PIN_8
/* defined the M2 Encoder A Phase pin: PB9 */
#define M2_B_TIMER_CH                  TIMER_CH_1
#define M2_B_GPIO_CLK                  RCU_GPIOB
#define M2_B_GPIO_PORT                 GPIOB
#define M2_B_GPIO_ALT_FUNC             GPIO_AF_2
#define M2_B_GPIO_PUPD                 GPIO_PUPD_NONE
#define M2_B_GPIO_PIN                  GPIO_PIN_9

#define ENCODER_COUNT_TIMER            TIMER6
#define ENCODER_COUNT_TIMER_CLK        RCU_TIMER6
#define ENCODER_COUNT_TIMER_IRQn       TIMER6_IRQn
#define ENCODER_COUNT_TIMER_IRQHANDLER TIMER6_IRQHandler

#define MOTOR_PULSE_PER_REVOLUTION 13u // 编码器单圈脉冲
#define MOTOR_REDUCTION_RATIO      20u // 电机减速比
#define MOTOR_WHEEL_STRAIGHT_MM    64u // 轮子直径，单位mm
#define MOTOR_WHEEL_CIRCUMFERENCE  3.1415926f * MOTOR_WHEEL_STRAIGHT_MM

#define	GET_BIT(x, bit)	((x & (1 << bit)) >> bit)
struct encoder_state
{
    volatile int32_t total_count;
    volatile int32_t last_count;
    volatile int32_t overflow_count;
    volatile uint8_t direction;
    volatile float speed;
};
typedef struct encoder_state encoder_state_t;

void bsp_encoder_init(void);
float get_car_distance(void);
float get_m1_speed(void);
float get_m2_speed(void);


#endif //__BSP_ENCODER_H__
