/*
 * 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
 * 开发板官网：www.lckfb.com
 * 技术支持常驻论坛，任何技术问题欢迎随时交流学习
 * 立创论坛：club.szlcsc.com
 * 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
 * 不靠卖板赚钱，以培养中国工程师为己任
 * Change Logs:
 * Date           Author       Notes
 * 2023-07-26     LCKFB-yzh    first version
 */
#include "board.h"
#include "bsp_encoder.h"
#include "positional_pid.h"
#include "bsp_motor.h"
#include "app_pid.h"

#define ABS(x) ((x > 0) ? x : -x)

// 具体位置信息分配如下所示，请对照实际地图理解
/*
           f                                       g
         ------                                 ------
           |                                       |
           |                                       |
           |                                       |
           |                                       |
           |4__________________3__________________5|
           |                   |                   |
           |                   |                   |
           |                   |                   |
           |                   |                   |
         __|___                |                 __|___
           e                   |                   h
                               |
                               |
                     |         |          |
                    c|_________2__________|d
                     |         |          |
                               |
                               |
                               |
                               |
                               |
                               |
                               |
                     |         |          |
                    a|.........1..........|b
                     |         |          |
                               |
                               |
                               |
                               |
                               |
                               |
                            .......
                               0
*/
typedef enum
{
    APP_CAR_CONTROL_INIT = 0,
    APP_CAR_CONTROL_WAIT_FOR_K210_NUMBER,
    APP_CAR_CONTROL_WAIT_FOR_LOAD,
    APP_CAR_CONTROL_FIRST_JUDGE,
    APP_CAR_CONTROL_GOTO_A,
    APP_CAR_CONTROL_GOTO_B,
    APP_CAR_CONTROL_SECOND_JUDGE,
    APP_CAR_CONTROL_2_GOTO_C,
    APP_CAR_CONTROL_2_GOTO_D,
    APP_CAR_CONTROL_THIRD_JUDGE,
    APP_CAR_CONTROL_FOURTH_JUDGE,
    APP_CAR_CONTROL_4_GOTO_E,
    APP_CAR_CONTROL_4_GOTO_F,
    APP_CAR_CONTROL_5_GOTO_G,
    APP_CAR_CONTROL_5_GOTO_H,

    APP_CAR_CONTROL_STOP,
} app_car_control_status_t;

static app_car_control_status_t app_car_control_status = APP_CAR_CONTROL_INIT;

// 小车1要传递给小车2的信息
static car1_to_car2_info_t car1_medical_ward_info;
// 小车2传递给小车1的信息
static car2_to_car1_info_t car2_medical_ward_info;

// 小车病房自身状态信息
static medical_ward_info_t medical_ward_info;

// 小车位置信息,对照第24行地图理解
// 可选：a,b,c,d,e,f,g,h,0,1,2,3,4,5
static char car_position = 0;

// 小车1控制小车2标志位
__attribute__((used)) static char car1_control_car2_flag = 0;

__attribute__((used)) static void set_car1_medical_ward_info_to_default(
    car1_to_car2_info_t *_car1_medical_ward_info);
__attribute__((used)) static void set_car2_medical_ward_info_to_default(
    car2_to_car1_info_t *_car2_medical_ward_info);

__attribute__((used)) static void
set_medical_ward_info_to_default(medical_ward_info_t *medical_ward_info);

uint8_t get_start_number(void);
int8_t get_k210_work_mode(void);

extern k210_data_t * get_k210_data(void);

extern int send_uart_k210_to_find_lines(void);
extern int send_uart_k210_to_find_number(void);
extern int car1_send_to_car2_info(car1_to_car2_info_t *_car1_medical_ward_info);
extern int car2_send_to_car1_info(car2_to_car1_info_t *_car2_medical_ward_info);
extern void get_car2_to_car1_info (car2_to_car1_info_t *_info);
extern void get_car1_to_car2_info(car1_to_car2_info_t *_info);

extern uint8_t get_medicine_state(void);
extern uint8_t is_another_car_online(void);


uint8_t get_start_number(void)
{
    static k210_data_t *_k210_data;

    _k210_data = get_k210_data();
    if (_k210_data->left_number == _k210_data->right_number)
    {
        return _k210_data->left_number;
    }
    else
    {
        return 0;
    }
}

/**
 -  @brief  小车状态信息软件定时器回调函数,串口定时发送给蓝牙模块
 -  @note   None
 -  @param  None
 -  @retval None
   */
void medical_ward_info_timer_callback(void)
{
#ifdef CURRENT_AS_CAR1
    car1_medical_ward_info.need_to_medical_ward_number =
        medical_ward_info.need_to_medical_ward_number;
    car1_medical_ward_info.medical_ward_a = medical_ward_info.medical_ward_a;
    car1_medical_ward_info.medical_ward_b = medical_ward_info.medical_ward_b;
    car1_medical_ward_info.medical_ward_c = medical_ward_info.medical_ward_c;
    car1_medical_ward_info.medical_ward_d = medical_ward_info.medical_ward_d;
    car1_medical_ward_info.medical_ward_e = medical_ward_info.medical_ward_e;
    car1_medical_ward_info.medical_ward_f = medical_ward_info.medical_ward_f;
    car1_medical_ward_info.medical_ward_g = medical_ward_info.medical_ward_g;
    car1_medical_ward_info.medical_ward_h = medical_ward_info.medical_ward_h;
    car1_medical_ward_info.car1_position = car_position;
    car1_medical_ward_info.car1_control_flag = car1_control_car2_flag;

    car1_send_to_car2_info(&car1_medical_ward_info);

    get_car2_to_car1_info(&car2_medical_ward_info);

#else
    car2_medical_ward_info.car2_position = car_position;

    car2_send_to_car1_info(&car2_medical_ward_info);

    get_car1_to_car2_info(&car1_medical_ward_info);

#endif
}

int car_control_k210_to_find_lines(void)
{
    enum _state_t
    {
        _START = 0,
        _WAIT_COMPLETE,
        _STOP
    };
    static enum _state_t state = _START;
    static uint32_t temp_systick;
    static k210_data_t *_k210_data;

	_k210_data = get_k210_data();
	
    switch (state)
    {
    case _START:
        send_uart_k210_to_find_lines();
        temp_systick = get_system_tick();
        state++;
        break;
    case _WAIT_COMPLETE:
        if (get_system_tick() - temp_systick >= 200)
        {
            if (_k210_data->work_mode == 0x00)
            {
                state++;
            }
            else
            {
                state--;
            }
        }
        break;
    case _STOP:
        state = _START;
        return 1;
    }
    return 0;
}

int car_control_k210_to_find_number(void)
{
    enum _state_t
    {
        _START = 0,
        _WAIT_COMPLETE,
        _STOP
    };
    static enum _state_t state = _START;
    static uint32_t temp_systick;
    static k210_data_t *_k210_data;
	
	_k210_data = get_k210_data();

    switch (state)
    {
    case _START:
        send_uart_k210_to_find_number();
        temp_systick = get_system_tick();
        state++;
        break;
    case _WAIT_COMPLETE:
        if (get_system_tick() - temp_systick >= 200)
        {
            if (_k210_data->work_mode == 0x01)
            {
                state++;
            }
            else
            {
                state--;
            }
        }
        break;
    case _STOP:
        state = _START;
        return 1;
    }
    return 0;
}

int car_go_until_crossing(float _speed,int32_t _encoder_count,int32_t _position_target)
{
    enum _state_t
    {
        _START = 0,
        _WAIT_DECELERATE,
        _DECELERATE,
        _POSITION_RUN,
        _STOP
    };
    static enum _state_t state = _START;

    static int32_t temp_start_distance = 0;
    static k210_data_t *_k210_data;

    _k210_data = get_k210_data();

    switch (state)
    {
    case _START:
        deactive_car_position_pid();
        active_car_red_lines_pid();

        set_car_red_lines_speed(_speed);
        temp_start_distance = get_m1_total_count();
        state++;
        break;
    case _WAIT_DECELERATE:
        if(ABS(get_m1_total_count() - temp_start_distance) >= _encoder_count)
        {
            set_car_red_lines_speed(0.0f);
            state++;
        }
        break;
    case _DECELERATE:
        set_car_red_lines_speed(1.5f);
        if (_k210_data->recognition == 0x02)
        {
            state++;
            set_car_red_lines_speed(0.0f);
            active_car_position_pid();
            deactive_car_red_lines_pid();
            car_go_position(_position_target);
        }
        break;
    case _POSITION_RUN:
        if (ABS(get_m1_position_target() - get_m1_position_measurement()) <= 50)
        {
//            printf("m1_position_target,m1_position_measurement:%d,%d\n",(int32_t)get_m1_position_target(),get_m1_position_measurement());
            state++;
        }
        break;
    case _STOP:
        set_car_red_lines_speed(0.0f);
        deactive_car_position_pid();
        active_car_red_lines_pid();
        state = _START;
        return 1;
    }
    return 0;
}

int car_go_until_crossing_with_num_recognition(float _speed,
                                               int32_t _encoder_count,
                                               int32_t _position_target,
                                               char *left_medical_number,
                                               char *right_medical_number)
{
    enum _state_t
    {
        _START = 0,
        _WAIT_DECELERATE,
        _DECELERATE,
        _ENTER_NUM_RECOGNITION,
        _RECOGNITION_NUMBER,
        _EXIT_NUM_RECOGNITION,
        _POSITION_RUN,
        _STOP
    };
    static enum _state_t state = _START;

    static int32_t temp_start_distance = 0;
    static uint32_t temp_sys_tick = 0;
    static k210_data_t *_k210_data;

    _k210_data = get_k210_data();

    switch (state)
    {
    case _START:
        deactive_car_position_pid();
        active_car_red_lines_pid();

        set_car_red_lines_speed(_speed);
        temp_start_distance = get_m1_total_count();
        state++;
        break;
    case _WAIT_DECELERATE:
        if (ABS(get_m1_total_count() - temp_start_distance) >= _encoder_count)
        {
            set_car_red_lines_speed(0.0f);
            state++;
        }
        break;
    case _DECELERATE:
        set_car_red_lines_speed(1.5f);
        if (_k210_data->recognition == 0x02)
        {
            state++;
            set_car_red_lines_speed(0.0f);
            active_car_position_pid();
            deactive_car_red_lines_pid();
        }
        break;
    case _ENTER_NUM_RECOGNITION:
        if (car_control_k210_to_find_number() == 1)
        {
            servo1_pwm_pulse_set(SERVO_FIND_NUMBER);
            temp_sys_tick = get_system_tick();
            state++;
        }
        break;
    case _RECOGNITION_NUMBER:
        if (_k210_data->left_number != _k210_data->right_number)
        {
            *left_medical_number = _k210_data->left_number;
            *right_medical_number = _k210_data->right_number;
            state++;
        }else
        {
            if(get_system_tick() - temp_sys_tick >= 50)
            {
                temp_sys_tick = get_system_tick();
                car_go_position(10);
            }
        }
        break;
    case _EXIT_NUM_RECOGNITION:
        if (car_control_k210_to_find_lines() == 1)
        {
            servo1_pwm_pulse_set(SERVO_FIND_LINE_ANGLE);
            state++;
            car_go_position(_position_target);
        }
        break;
    case _POSITION_RUN:
        if (ABS(get_m1_position_target() - get_m1_position_measurement()) <= 50)
        {
//            printf("m1_position_target,m1_position_measurement:%d,%d\n",
//                   (int32_t)get_m1_position_target(),
//                   get_m1_position_measurement());
            state++;
        }
        break;
    case _STOP:
        set_car_red_lines_speed(0.0f);
        deactive_car_position_pid();
        active_car_red_lines_pid();
        state = _START;
        return 1;
    }
    return 0;
}

int car_go_until_sickroom(float _speed, int32_t _encoder_count,
                          int32_t _position_target)
{
    enum _state_t
    {
        _START = 0,
        _WAIT_DECELERATE,
        _DECELERATE,
        _POSITION_RUN,
        _STOP
    };
    static enum _state_t state = _START;

    static int32_t temp_start_distance = 0;
    static uint32_t temp_systick;
    static k210_data_t *_k210_data;

    _k210_data = get_k210_data();

    switch (state)
    {
    case _START:
        deactive_car_position_pid();
        active_car_red_lines_pid();

        set_car_red_lines_speed(_speed);
        temp_start_distance = get_m1_total_count();

        state++;
        break;
    case _WAIT_DECELERATE:
        if (get_m1_total_count() - temp_start_distance >= _encoder_count)
        {
            state++;
        }
        break;
    case _DECELERATE:
        set_car_red_lines_speed(1.5f);
        if (_k210_data->recognition == 0x01)
        {
            state++;
            set_car_red_lines_speed(0.0f);
            active_car_position_pid();
            deactive_car_red_lines_pid();
            car_go_position(_position_target);
            temp_systick = get_system_tick();
        }
        break;
    case _POSITION_RUN:
        if (get_system_tick() - temp_systick >= 300)
        {
            state++;
        }
        else if (_position_target == 0)
        {
            state++;
        }
        break;
    case _STOP:
        set_car_red_lines_speed(0.0f);
        deactive_car_position_pid();
        deactive_car_red_lines_pid();
        state = _START;
        return 1;
    }
    return 0;
}

// 当前位置左转
static int car_spin(int32_t _count,float _speed,uint32_t _wait_pos_tick,uint32_t _wait_line_tick)
{
    static uint32_t temp_systick;

    enum _state_t
    {
        _START = 0,
        _WAIT_COMPLETE,
        _START_FINE_LINE,
        _STOP
    };

    static enum _state_t state = _START;

    switch (state)
    {
    case _START:
        deactive_car_red_lines_pid();
        active_car_position_pid();

        car_spin_position(_count);
        set_car_position_max_speed(_speed);

        temp_systick = get_system_tick();

        state++;
        break;
    case _WAIT_COMPLETE:
        if (get_system_tick() - temp_systick >= _wait_pos_tick)
        {
            deactive_car_position_pid();
            active_car_red_lines_pid();
            temp_systick = get_system_tick();
            state++;
        }
        break;
    case _START_FINE_LINE:
        if (get_system_tick() - temp_systick >= _wait_line_tick)
        {
            state++;
        }
        break;
    case _STOP:
        set_car_red_lines_speed(0.0f);
        state = _START;
        return 1;
    }
    return 0;
}



extern uint8_t get_medicine_state(void);

void app_car_control_run(void)
{
    static uint8_t a_temp_step = 0;
    static uint8_t b_temp_step = 0;
    static uint8_t c_temp_step = 0;
    static uint8_t d_temp_step = 0;

    static uint8_t third_temp_step = 0;
    static uint8_t fourth_temp_step = 0;

    static uint8_t e_temp_step = 0;
    static uint8_t f_temp_step = 0;
    static uint8_t g_temp_step = 0;
    static uint8_t h_temp_step = 0;

    switch (app_car_control_status)
    {
    case APP_CAR_CONTROL_INIT:
        if (car_control_k210_to_find_number() == 1)
        {
            app_car_control_status++;
        }
        break;
    case APP_CAR_CONTROL_WAIT_FOR_K210_NUMBER:

        if (get_start_number() != 0)
        {
            medical_ward_info.need_to_medical_ward_number = get_start_number();
        }
        if (0 != medical_ward_info.need_to_medical_ward_number)
        {
            if (car_control_k210_to_find_lines() == 1)
            {
                buzzer_beep_tone(4000, 100, 100, 100);
                app_car_control_status++;
            }
        }

        break;
    case APP_CAR_CONTROL_WAIT_FOR_LOAD:
        if (get_medicine_state() == (uint8_t)1)
        {
            buzzer_beep_tone(4000, 100, 100, 100);
            app_car_control_status++;
        }
        break;
    case APP_CAR_CONTROL_FIRST_JUDGE:
        if (get_start_number() == 1)
        {
            app_car_control_status = APP_CAR_CONTROL_GOTO_A;
        }
        else if (get_start_number() == 2)
        {
            app_car_control_status = APP_CAR_CONTROL_GOTO_B;
        }
        else // 不是近端病房
        {
            app_car_control_status = APP_CAR_CONTROL_SECOND_JUDGE;
        }
        break;
    case APP_CAR_CONTROL_GOTO_A:

        switch (a_temp_step)
        {
        case 0:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 3000, 1000))
            {
                car_position = '1';
                a_temp_step++;
            }
            break;
        case 1:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                a_temp_step++;
            }
            break;
        case 2:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 200))
            {
                // 到达病房了
                car_position = 'a';
                bsp_led_on(LED_RED);
                a_temp_step++;
            }
            break;
        case 3:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                a_temp_step++;
            }
            break;
        case 4:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                a_temp_step++;
            }
            break;
        case 5:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '1';
                a_temp_step++;
            }
            break;
        case 6:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                a_temp_step++;
            }
            break;
        case 7:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 3000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                a_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        default:
            break;
        }
        break;
    case APP_CAR_CONTROL_GOTO_B:

        switch (b_temp_step)
        {
        case 0:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 3000, 1000))
            {
                car_position = '1';
                b_temp_step++;
            }
            break;
        case 1:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                b_temp_step++;
            }
            break;
        case 2:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 200))
            {
                // 到达病房了
                car_position = 'b';
                bsp_led_on(LED_RED);
                b_temp_step++;
            }
            break;
        case 3:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                b_temp_step++;
            }
            break;
        case 4:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                b_temp_step++;
            }
            break;
        case 5:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '1';
                b_temp_step++;
            }
            break;
        case 6:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                b_temp_step++;
            }
            break;
        case 7:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 3000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                b_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        default:
            break;
        }
        break;
    case APP_CAR_CONTROL_SECOND_JUDGE:
        if (car_go_until_crossing_with_num_recognition(
                CAR_DEFAULT_SPEED, 7000, 1100,
                &medical_ward_info.medical_ward_c,
                &medical_ward_info.medical_ward_d)
            == 1)
        {
            car_position = '2';
            if (medical_ward_info.medical_ward_c
                == medical_ward_info.need_to_medical_ward_number)
            {
                printf("to left ward\n");
                app_car_control_status = APP_CAR_CONTROL_2_GOTO_C;
            }
            else if (medical_ward_info.medical_ward_d
                     == medical_ward_info.need_to_medical_ward_number)
            {
                printf("to right ward\n");
                app_car_control_status = APP_CAR_CONTROL_2_GOTO_D;
            }
            else
            {
                app_car_control_status = APP_CAR_CONTROL_THIRD_JUDGE;
            }
        }

        break;
    case APP_CAR_CONTROL_2_GOTO_C:
        switch (c_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                c_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 200))
            {
                // 到达病房了
                car_position = 'c';
                bsp_led_on(LED_RED);
                c_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                c_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                c_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '2';
                c_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                c_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 6000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                c_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_2_GOTO_D:
        switch (d_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                d_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 200))
            {
                // 到达病房了
                car_position = 'd';
                bsp_led_on(LED_RED);
                d_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                d_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                d_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '2';
                d_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                d_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 6000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                d_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_THIRD_JUDGE:

        switch (third_temp_step)
        {
        case 0:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 3000, 1300))
            {
                car_position = '3';
                third_temp_step++;
            }
            break;
        case 1:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                third_temp_step++;
            }
            break;
        case 2:
            if (car_go_until_crossing_with_num_recognition(
                    CAR_DEFAULT_SPEED, 3000, 1300,
                    &medical_ward_info.medical_ward_e,
                    &medical_ward_info.medical_ward_f)
                == 1)
            {
                car_position = '4';
                if (medical_ward_info.medical_ward_e
                    == medical_ward_info.need_to_medical_ward_number)
                {
                    printf("to left ward\n");
                    app_car_control_status = APP_CAR_CONTROL_4_GOTO_E;
                }
                else if (medical_ward_info.medical_ward_f
                         == medical_ward_info.need_to_medical_ward_number)
                {
                    printf("to right ward\n");
                    app_car_control_status = APP_CAR_CONTROL_4_GOTO_F;
                }
                else
                {
                    app_car_control_status = APP_CAR_CONTROL_FOURTH_JUDGE;
                }
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_4_GOTO_E:
        switch (e_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                e_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 300))
            {
                // 到达病房了
                car_position = 'e';
                bsp_led_on(LED_RED);
                e_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                e_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                e_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 500, 1200))
            {
                car_position = '4';
                e_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                e_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 1500, 1200))
            {
                car_position = '3';
                e_temp_step++;
            }
            break;
        case 7:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                e_temp_step++;
            }
            break;
        case 8:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 10000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                e_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_4_GOTO_F:
        switch (f_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                f_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 300))
            {
                // 到达病房了
                car_position = 'f';
                bsp_led_on(LED_RED);
                f_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                f_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                f_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '4';
                f_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                f_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '3';
                f_temp_step++;
            }
            break;
        case 7:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                f_temp_step++;
            }
            break;
        case 8:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 10000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                f_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_FOURTH_JUDGE:

        switch (fourth_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                fourth_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_crossing_with_num_recognition(
                    CAR_DEFAULT_SPEED, 5000, 1300,
                    &medical_ward_info.medical_ward_g,
                    &medical_ward_info.medical_ward_h)
                == 1)
            {
                car_position = '5';
                if (medical_ward_info.medical_ward_g
                    == medical_ward_info.need_to_medical_ward_number)
                {
                    printf("to left ward\n");
                    app_car_control_status = APP_CAR_CONTROL_5_GOTO_G;
                }
                else if (medical_ward_info.medical_ward_h
                         == medical_ward_info.need_to_medical_ward_number)
                {
                    printf("to right ward\n");
                    app_car_control_status = APP_CAR_CONTROL_5_GOTO_H;
                }
                else
                {
                    // no pharmacy to go
                    app_car_control_status = APP_CAR_CONTROL_5_GOTO_G;
                }
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_5_GOTO_G:
        switch (g_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                g_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 300))
            {
                // 到达病房了
                car_position = 'g';
                bsp_led_on(LED_RED);
                g_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                g_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                g_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '5';
                g_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                g_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 1500, 1200))
            {
                car_position = '3';
                g_temp_step++;
            }
            break;
        case 7:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                g_temp_step++;
            }
            break;
        case 8:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 10000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                e_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    case APP_CAR_CONTROL_5_GOTO_H:
        switch (h_temp_step)
        {
        case 0:
            if (car_spin(CAR_CONTROL_SPIN_RIGHT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                h_temp_step++;
            }
            break;
        case 1:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 1500, 300))
            {
                // 到达病房了
                car_position = 'h';
                bsp_led_on(LED_RED);
                h_temp_step++;
            }
            break;
        case 2:
            if (get_medicine_state() == (uint8_t)0)
            {
                bsp_led_off(LED_RED);
                buzzer_beep_tone(4000, 100, 100, 100);
                h_temp_step++;
            }
            break;
        case 3:
            if (car_spin(CAR_CONTROL_SPIN_RETURN_NUM, CAR_DEFAULT_SPEED, 500,
                         300))
            {
                h_temp_step++;
            }
            break;
        case 4:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 300, 1200))
            {
                car_position = '5';
                h_temp_step++;
            }
            break;
        case 5:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                h_temp_step++;
            }
            break;
        case 6:
            if (car_go_until_crossing(CAR_DEFAULT_SPEED, 1500, 1200))
            {
                car_position = '3';
                h_temp_step++;
            }
            break;
        case 7:
            if (car_spin(CAR_CONTROL_SPIN_LEFT_NUM, CAR_DEFAULT_SPEED, 250,
                         300))
            {
                h_temp_step++;
            }
            break;
        case 8:
            if (car_go_until_sickroom(CAR_DEFAULT_SPEED, 10000, 100))
            {
                // 回到药房了
                car_position = '0';
                bsp_led_on(LED_GREEN);
                h_temp_step++;
                app_car_control_status = APP_CAR_CONTROL_STOP;
            }
            break;
        }
        break;
    default:
        break;
    }


}
