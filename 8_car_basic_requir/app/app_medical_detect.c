/*
 * 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
 * 开发板官网：www.lckfb.com
 * 技术支持常驻论坛，任何技术问题欢迎随时交流学习
 * 立创论坛：club.szlcsc.com
 * 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
 * 不靠卖板赚钱，以培养中国工程师为己任
 * Change Logs:
 * Date           Author       Notes
 * 2023-07-26     LCKFB-yzh    first version
 */

#include "board.h"
typedef enum
{
    APP_MEDICAL_DETECT_INIT = 0,
    APP_MEDICAL_DETECT_RUN,
    APP_MEDICAL_DETECT_CONFIRM,
    APP_MEDICAL_DETECT_STOP
} app_medical_detect_status_t;

static app_medical_detect_status_t app_medical_detect_status = APP_MEDICAL_DETECT_INIT;

#define KEY0_PIN                         GPIO_PIN_12
#define KEY0_GPIO_PORT                   GPIOG
#define KEY0_GPIO_CLK                    RCU_GPIOG

void key0_gpio_init(void)
{
    /* enable the led clock */
    rcu_periph_clock_enable(KEY0_GPIO_CLK);
    /* configure led GPIO port */
    gpio_mode_set(KEY0_GPIO_PORT, GPIO_MODE_INPUT, GPIO_PUPD_PULLDOWN, KEY0_PIN);
}

__attribute__((used)) static uint8_t medicine_state = 0;

void app_medical_detect_run(void)
{
    static uint32_t enter_confirm_systick = 0;

    switch (app_medical_detect_status)
    {
    case APP_MEDICAL_DETECT_INIT:
        key0_gpio_init();
        app_medical_detect_status++;
        break;
    case APP_MEDICAL_DETECT_RUN:
        if (SET == gpio_input_bit_get(KEY0_GPIO_PORT, KEY0_PIN))
        {
            app_medical_detect_status++;
            enter_confirm_systick = get_system_tick();
        }
        else
        {
            medicine_state = 0;
        }
        break;
    case APP_MEDICAL_DETECT_CONFIRM:
        if (SET == gpio_input_bit_get(KEY0_GPIO_PORT, KEY0_PIN))
        {
            if (get_system_tick() - enter_confirm_systick > 100)
            {
                medicine_state = 1;
            }
            else
            {
                medicine_state = 0;
            }
        }
        else
        {
            medicine_state = 0;
            app_medical_detect_status--;
        }
        break;
    case APP_MEDICAL_DETECT_STOP:

        break;
    default:
        break;
    }
}

uint8_t get_medicine_state(void)
{
    return medicine_state;
}
