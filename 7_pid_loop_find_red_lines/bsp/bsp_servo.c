/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
 */
#include <board.h>
#include <bsp_servo.h>
#include <car_config.h>

static void servo_pwm_gpio_init(void)
{
    // servo pwm gpio init
    rcu_periph_clock_enable(SERVO_1_GPIO_CLK);
    rcu_periph_clock_enable(SERVO_2_GPIO_CLK);

    gpio_mode_set(SERVO_1_GPIO_PORT, GPIO_MODE_AF, SERVO_1_GPIO_PUPD,
                  SERVO_1_GPIO_PIN);
    gpio_mode_set(SERVO_2_GPIO_PORT, GPIO_MODE_AF, SERVO_2_GPIO_PUPD,
                  SERVO_2_GPIO_PIN);

    rcu_periph_clock_enable(RCU_SYSCFG);

    gpio_af_set(SERVO_1_GPIO_PORT, SERVO_1_GPIO_ALT_FUNC, SERVO_1_GPIO_PIN);
    gpio_af_set(SERVO_2_GPIO_PORT, SERVO_2_GPIO_ALT_FUNC, SERVO_2_GPIO_PIN);

    gpio_output_options_set(SERVO_1_GPIO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, SERVO_1_GPIO_PIN);
    gpio_output_options_set(SERVO_2_GPIO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, SERVO_2_GPIO_PIN);
    return;
}

static void servo_timer_init(void)
{
    // motor1 pwm timer init
    timer_parameter_struct timer_param_type;
    timer_oc_parameter_struct timer_oc_init_struct;

    rcu_periph_clock_enable(SERVO_TIMER_CLK);
    timer_deinit(SERVO_TIMER);
    timer_struct_para_init(&timer_param_type);

    timer_param_type.alignedmode = TIMER_COUNTER_EDGE;
    timer_param_type.clockdivision = TIMER_CKDIV_DIV1;
    timer_param_type.counterdirection = TIMER_COUNTER_UP;
    timer_param_type.period = 1200-1;   //50Hz
    timer_param_type.prescaler = 4000-1;   //240Mhz/4000=60000Hz
    timer_param_type.repetitioncounter = 0;
    timer_init(SERVO_TIMER, &timer_param_type);

    timer_channel_output_struct_para_init(&timer_oc_init_struct);
    timer_oc_init_struct.ocpolarity = TIMER_OC_POLARITY_HIGH;
    timer_oc_init_struct.outputstate = TIMER_CCX_ENABLE;
    timer_oc_init_struct.outputnstate = TIMER_CCXN_DISABLE;
    timer_oc_init_struct.ocnpolarity = TIMER_OCN_POLARITY_HIGH;
    timer_oc_init_struct.ocidlestate = TIMER_OC_IDLE_STATE_LOW;
    timer_oc_init_struct.ocnidlestate = TIMER_OCN_IDLE_STATE_LOW;

    timer_channel_output_config(SERVO_TIMER, TIMER_CH_0, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(SERVO_TIMER, TIMER_CH_0, SERVO_FIND_LINE_ANGLE);
    timer_channel_output_mode_config(SERVO_TIMER,TIMER_CH_0,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(SERVO_TIMER,TIMER_CH_0,TIMER_OC_SHADOW_DISABLE);


    timer_channel_output_config(SERVO_TIMER, TIMER_CH_1, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(SERVO_TIMER, TIMER_CH_1, 90);
    timer_channel_output_mode_config(SERVO_TIMER,TIMER_CH_1,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(SERVO_TIMER,TIMER_CH_1,TIMER_OC_SHADOW_DISABLE);
    timer_auto_reload_shadow_enable(SERVO_TIMER);

    //高级定时器必须要用下面这条语句
    timer_primary_output_config(SERVO_TIMER,ENABLE);

    timer_enable(SERVO_TIMER);

    return;
}

void servo1_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(SERVO_TIMER, TIMER_CH_0, pulse);
}
void servo2_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(SERVO_TIMER, TIMER_CH_1, pulse);
}


void servo_init(void)
{
    servo_pwm_gpio_init();
    servo_timer_init();
    return;
}

