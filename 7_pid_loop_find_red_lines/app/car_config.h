#ifndef _CAR_CONFIG_H
#define _CAR_CONFIG_H

//定义舵机巡红线时角度
#define SERVO_FIND_LINE_ANGLE 85
//定义舵机识别数字时角度
#define SERVO_FIND_NUMBER 75

//定义这个就是1车，不定义这个（注释掉）就是2车
#define CURRENT_AS_CAR1


//以下是调试时使用的宏定义
#define DEBUG_PID_MOTOR1_SPEED 0
#define DEBUG_PID_MOTOR2_SPEED 0

#define DEBUG_PID_MOTOR1_POSITION 0
#define DEBUG_PID_MOTOR2_POSITION 0

#define DEBUG_PROTOCOL_K210_data 0
#define DEBUG_PROTOCOL_BLE_data 0

#define DEBUG_PID_FIND_RED_LINE 1

#endif
