/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-25     LCKFB-yzh    first version
*/
#include "board.h"
#include "self_data_type.h"
#include "upacker.h"
#include "ringbuffer.h"

//uart2 和K210通讯，和K210的串口相连
//uart5 做双车通讯，和蓝牙模块相连
typedef enum
{
   APP_PROTOCOL_INIT = 0,
   APP_PROTOCOL_RUN,
   APP_PROTOCOL_STOP
} app_protocol_status_t;

static app_protocol_status_t app_protocol_status = APP_PROTOCOL_INIT;

static upacker_inst k210_msg_packer;
static upacker_inst ble_msg_packer;


static k210_data_t k210_data;

//串口ringbuffer配置区域
#define UART2_RING_BUFFER_LEN 256
#define UART5_RING_BUFFER_LEN 256

static ring_buffer_t rb_uart2;
static ring_buffer_t rb_uart5;

static uint8_t uart2_rb_buff[UART2_RING_BUFFER_LEN];
static uint8_t uart5_rb_buff[UART5_RING_BUFFER_LEN];


// 小车2接收的信息
__attribute__((used)) static car1_to_car2_info_t receive_car1_medical_ward_info;
// 小车1接收的信息
__attribute__((used)) static car2_to_car1_info_t receive_car2_medical_ward_info;


#define UART2_DMA_RCU            RCU_DMA0
#define UART2_DMA                DMA0
#define UART2_DMA_CH             DMA_CH1
#define UART2_DMA_CH_IRQ         DMA0_Channel1_IRQn
#define UART2_DMA_CH_IRQ_HANDLER DMA0_Channel1_IRQHandler

#define UART5_DMA_RCU            RCU_DMA1
#define UART5_DMA                DMA1
#define UART5_DMA_CH             DMA_CH1
#define UART5_DMA_CH_IRQ         DMA1_Channel1_IRQn
#define UART5_DMA_CH_IRQ_HANDLER DMA1_Channel1_IRQHandler

#define UART2_RECEIVE_LENGTH     16
static uint8_t uart2_recv_buff[UART2_RECEIVE_LENGTH];

// 车1，车2的接收数据大小不一样
#ifdef CURRENT_AS_CAR1
// example:55 01 40 55 00
#define UART5_RECEIVE_LENGTH 5
#else
// example:55 0B C0 5D 00 01 02 00 00 00 00 00 00 00 00
#define UART5_RECEIVE_LENGTH 15
#endif
static uint8_t uart5_recv_buff[UART5_RECEIVE_LENGTH];

// 上一次接收到另一个车传过来数据的时间
static uint32_t last_receive_ble_tick = 0;
// 另一辆车是否在线
static uint8_t is_another_car_online_flag;
uint8_t is_another_car_online(void) { return is_another_car_online_flag; }

// 上一次接收到K210传过来数据的时间
static uint32_t last_receive_k210_tick = 0;
// 另一辆车是否在线
static uint8_t is_k210_online_flag;
uint8_t is_k210_online(void) { return is_k210_online_flag; }

void update_ble_online_flag(void)
{
    if ((get_system_tick() - last_receive_ble_tick) >= 200)
    {
        is_another_car_online_flag = 0;
    }
    else
    {
        is_another_car_online_flag = 1;
    }
}
void update_k210_online_flag(void)
{
    if ((get_system_tick() - last_receive_k210_tick) >= 200)
    {
        is_k210_online_flag = 0;
    }
    else
    {
        is_k210_online_flag = 1;
    }
}

void uart2_dma_config(void)
{
    dma_single_data_parameter_struct dma_init_struct;
    rcu_periph_clock_enable(UART2_DMA_RCU);

    dma_deinit(UART2_DMA, UART2_DMA_CH);

    dma_init_struct.periph_addr = (uint32_t)&USART_DATA(USART2);
    dma_init_struct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;
    dma_init_struct.memory0_addr = (uint32_t)uart2_recv_buff;
    dma_init_struct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;
    dma_init_struct.periph_memory_width = DMA_PERIPH_WIDTH_8BIT;
    dma_init_struct.circular_mode = DMA_CIRCULAR_MODE_DISABLE;
    dma_init_struct.direction = DMA_PERIPH_TO_MEMORY;
    dma_init_struct.number = UART2_RECEIVE_LENGTH;
    dma_init_struct.priority = DMA_PRIORITY_ULTRA_HIGH;

    dma_single_data_mode_init(UART2_DMA, UART2_DMA_CH, &dma_init_struct);

    dma_channel_subperipheral_select(UART2_DMA, UART2_DMA_CH, DMA_SUBPERI4);

    dma_channel_enable(UART2_DMA, UART2_DMA_CH);

    dma_interrupt_enable(UART2_DMA, UART2_DMA_CH, DMA_CHXCTL_FTFIE);
    nvic_irq_enable(UART2_DMA_CH_IRQ, 1, 1);

    usart_dma_receive_config(USART2, USART_RECEIVE_DMA_ENABLE);

    usart_interrupt_disable(USART2, USART_INT_RBNE);

    usart_flag_clear(USART5, USART_FLAG_IDLE);
    usart_interrupt_enable(USART2, USART_INT_IDLE);
}

void UART2_DMA_CH_IRQ_HANDLER(void)
{
    if (dma_interrupt_flag_get(UART2_DMA, UART2_DMA_CH, DMA_INT_FLAG_FTF)
        == SET)
    {
        dma_interrupt_flag_clear(UART2_DMA, UART2_DMA_CH, DMA_INT_FLAG_FTF);
    }
}

void uart5_dma_config(void)
{
    dma_single_data_parameter_struct dma_init_struct;
    rcu_periph_clock_enable(UART5_DMA_RCU);

    dma_deinit(UART5_DMA, UART5_DMA_CH);

    dma_init_struct.periph_addr = (uint32_t)&USART_DATA(USART5);
    dma_init_struct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;
    dma_init_struct.memory0_addr = (uint32_t)uart5_recv_buff;
    dma_init_struct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;
    dma_init_struct.periph_memory_width = DMA_PERIPH_WIDTH_8BIT;
    dma_init_struct.circular_mode = DMA_CIRCULAR_MODE_DISABLE;
    dma_init_struct.direction = DMA_PERIPH_TO_MEMORY;
    dma_init_struct.number = UART5_RECEIVE_LENGTH;
    dma_init_struct.priority = DMA_PRIORITY_ULTRA_HIGH;

    dma_single_data_mode_init(UART5_DMA, UART5_DMA_CH, &dma_init_struct);

    dma_channel_subperipheral_select(UART5_DMA, UART5_DMA_CH, DMA_SUBPERI5);

    dma_channel_enable(UART5_DMA, UART5_DMA_CH);

    dma_interrupt_enable(UART5_DMA, UART5_DMA_CH, DMA_CHXCTL_FTFIE);
    nvic_irq_enable(UART5_DMA_CH_IRQ, 1, 1);

    usart_dma_receive_config(USART5, USART_RECEIVE_DMA_ENABLE);

    usart_interrupt_disable(USART5, USART_INT_RBNE);

    usart_flag_clear(USART5, USART_FLAG_IDLE);
    usart_interrupt_enable(USART5, USART_INT_IDLE);
}

void UART5_DMA_CH_IRQ_HANDLER(void)
{
    if (dma_interrupt_flag_get(UART5_DMA, UART5_DMA_CH, DMA_INT_FLAG_FTF)
        == SET)
    {
        dma_interrupt_flag_clear(UART5_DMA, UART5_DMA_CH, DMA_INT_FLAG_FTF);
    }
}

/**
 * @brief  k210发送接口
 * @note
 * @param  *buff:
 * @param  len:
 * @retval None
 */
static void k210_protocol_send(uint8_t *buff, uint16_t len)
{
    for(uint16_t i = 0; i < len; i++)
    {
        usart_data_transmit(COM_UART2, (uint8_t)buff[i]);
        while(RESET == usart_flag_get(COM_UART2, USART_FLAG_TBE));
    }
}

void USART2_IRQHandler(void)
{
    if (usart_interrupt_flag_get(USART2, USART_INT_FLAG_IDLE) == SET)
    {
        usart_data_receive(USART2);

        dma_channel_disable(UART2_DMA, UART2_DMA_CH);
        ring_buffer_write(&rb_uart2, (uint8_t *)&uart2_recv_buff,
                          UART2_RECEIVE_LENGTH);
        uart2_dma_config();
    }
}

void USART5_IRQHandler(void)
{
    if (usart_interrupt_flag_get(USART5, USART_INT_FLAG_IDLE) == SET)
    {
        usart_data_receive(USART5);

        dma_channel_disable(UART5_DMA, UART5_DMA_CH);
        ring_buffer_write(&rb_uart5, (uint8_t *)&uart5_recv_buff,
                          UART5_RECEIVE_LENGTH);
        uart5_dma_config();

    }

}

uint32_t receive_count = 0;
/**
 * @brief  k210消息解析回调
 * @note
 * @param  *buff:
 * @param  size:
 * @retval None
 */
static void k210_protocol_handle_cb(uint8_t *buff, uint16_t len)
{
    static uint32_t temp_tick;
    // 接收到payload,到了这里下面的数据就是已经校验过的了
    receive_count++;
    if (get_system_tick() - temp_tick >= 1000)
    {
		// printf("rec:%d\n",receive_count);
        receive_count = 0;
        temp_tick = get_system_tick();
    }

    last_receive_k210_tick = get_system_tick();

    k210_data.work_mode = buff[0];
    k210_data.recognition = buff[1];
    k210_data.top_block_offset = buff[2] + (buff[3] << 8);
    k210_data.center_block_offset = buff[4] + (buff[5] << 8);
    k210_data.left_block_offset = buff[6] + (buff[7] << 8);
    k210_data.right_block_offset = buff[8] + (buff[9] << 8);
    k210_data.left_number = buff[10];
    k210_data.right_number = buff[11];

#if DEBUG_PROTOCOL_K210_data == 1
    printf("mode,reco,top,center,left,right,left_num,right_num:%d,%d,%d,%d,%d,%"
           "d,%d,%d\n",
           k210_data.work_mode, k210_data.recognition,
           k210_data.top_block_offset, k210_data.center_block_offset,
           k210_data.left_block_offset, k210_data.right_block_offset,
           k210_data.left_number, k210_data.right_number);
#endif
}

//K210 的数据不是在中断里改变的，可以不做临界区保护
k210_data_t * get_k210_data(void)
{
    return &k210_data;
}

int send_uart_k210_to_find_lines(void)
{
    static uint8_t temp = 0x00;
    upacker_pack(&k210_msg_packer, &temp, 1);
    return 0;
}
int send_uart_k210_to_find_number(void)
{
    static uint8_t temp = 0x01;
    upacker_pack(&k210_msg_packer, &temp, 1);
    return 0;
}

int car1_send_to_car2_info(car1_to_car2_info_t *_car1_medical_ward_info)
{
    static uint8_t temp[11];
    temp[0] = _car1_medical_ward_info->need_to_medical_ward_number;
    temp[1] = _car1_medical_ward_info->medical_ward_a;
    temp[2] = _car1_medical_ward_info->medical_ward_b;
    temp[3] = _car1_medical_ward_info->medical_ward_c;
    temp[4] = _car1_medical_ward_info->medical_ward_d;
    temp[5] = _car1_medical_ward_info->medical_ward_e;
    temp[6] = _car1_medical_ward_info->medical_ward_f;
    temp[7] = _car1_medical_ward_info->medical_ward_g;
    temp[8] = _car1_medical_ward_info->medical_ward_h;
    temp[9] = _car1_medical_ward_info->car1_position;
    temp[10] = _car1_medical_ward_info->car1_control_flag;
    upacker_pack(&ble_msg_packer, temp, 11);
    return 0;
}

int car2_send_to_car1_info(car2_to_car1_info_t *_car2_medical_ward_info)
{
    static uint8_t temp;
    temp = _car2_medical_ward_info->car2_position;
    upacker_pack(&ble_msg_packer, &temp, 1);
    return 0;
}

static uint8_t receive_char;

/**
 * @brief  ble发送接口
 * @note
 * @param  *d:
 * @param  size:
 * @retval None
 */
static void ble_protocol_send(uint8_t *buff, uint16_t len)
{
    for(uint16_t i = 0; i < len; i++)
    {
        usart_data_transmit(COM_UART5, (uint8_t)buff[i]);
        while(RESET == usart_flag_get(COM_UART5, USART_FLAG_TBE));
    }
}

extern void
update_car1_medical_ward_info(car1_to_car2_info_t *_car1_medical_ward_info);
extern void
update_car2_medical_ward_info(car2_to_car1_info_t *_car2_medical_ward_info);
/**
 * @brief  ble消息解析回调
 * @note
 * @param  *buff:
 * @param  size:
 * @retval None
 */
static void ble_protocol_handle_cb(uint8_t *buff, uint16_t len)
{

    last_receive_ble_tick = get_system_tick();
#ifdef CURRENT_AS_CAR1
    // example:55 01 40 55 00
    receive_car2_medical_ward_info.car2_position = buff[0];

    // mcn_publish(MCN_HUB(car2_to_car1_info_topic),
    //             &receive_car2_medical_ward_info);
#else
    // example:55 0B C0 5D 00 01 02 00 00 00 00 00 00 00 00
    receive_car1_medical_ward_info.need_to_medical_ward_number = buff[0];
    receive_car1_medical_ward_info.medical_ward_a = buff[1];
    receive_car1_medical_ward_info.medical_ward_b = buff[2];
    receive_car1_medical_ward_info.medical_ward_c = buff[3];
    receive_car1_medical_ward_info.medical_ward_d = buff[4];
    receive_car1_medical_ward_info.medical_ward_e = buff[5];
    receive_car1_medical_ward_info.medical_ward_f = buff[6];
    receive_car1_medical_ward_info.medical_ward_g = buff[7];
    receive_car1_medical_ward_info.medical_ward_h = buff[8];
    receive_car1_medical_ward_info.car1_position = buff[9];
    receive_car1_medical_ward_info.car1_control_flag = buff[10];

    // mcn_publish(MCN_HUB(car1_to_car2_info_topic),
    //             &receive_car1_medical_ward_info);
#endif
}

void app_protocol_run(void)
{
    switch (app_protocol_status)
    {
    case APP_PROTOCOL_INIT:

        ring_buffer_init(&rb_uart2, uart2_rb_buff, UART2_RING_BUFFER_LEN);
        ring_buffer_init(&rb_uart5, uart5_rb_buff, UART5_RING_BUFFER_LEN);

        // init k210 packer
        upacker_init(&k210_msg_packer, k210_protocol_handle_cb,
                     k210_protocol_send);
        // init ble packer
        upacker_init(&ble_msg_packer, ble_protocol_handle_cb,
                     ble_protocol_send);

        uart2_dma_config();
        uart5_dma_config();

        app_protocol_status++;
        break;
    case APP_PROTOCOL_RUN:

        if (ring_buffer_available_read(&rb_uart2) >= 1)
        {
            ring_buffer_read(&rb_uart2, &receive_char, 1);

            upacker_unpack(&k210_msg_packer, &receive_char, 1);
        }
        if (ring_buffer_available_read(&rb_uart5) >= 1)
        {
            ring_buffer_read(&rb_uart5, &receive_char, 1);

            upacker_unpack(&ble_msg_packer, &receive_char, 1);
        }

        update_ble_online_flag();

        update_k210_online_flag();

        break;
    case APP_PROTOCOL_STOP:

        break;
    default:
        break;
    }
}
