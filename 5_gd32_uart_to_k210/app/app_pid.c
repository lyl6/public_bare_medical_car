/*
 * 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
 * 开发板官网：www.lckfb.com
 * 技术支持常驻论坛，任何技术问题欢迎随时交流学习
 * 立创论坛：club.szlcsc.com
 * 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
 * 不靠卖板赚钱，以培养中国工程师为己任
 * Change Logs:
 * Date           Author       Notes
 * 2023-07-24     LCKFB-yzh    first version
 */
#include "board.h"
#include "bsp_encoder.h"
#include "positional_pid.h"
#include "bsp_motor.h"

typedef enum
{
    APP_PID_INIT = 0,
    APP_PID_RUN,
    APP_PID_STOP
} app_pid_status_t;

static app_pid_status_t app_pid_status = APP_PID_INIT;

// 小车电机位置环开启状态
__attribute__((used)) static char car_position_pid_enable_flag = 1;
// 小车巡红线环开启状态
__attribute__((used)) static char car_find_red_line_enable_flag = 0;


//速度环相关变量
static positional_pid_params_t motor1_speed_pid;
static positional_pid_params_t motor2_speed_pid;

static float motor1_speed_target = 0.0f;
static float motor2_speed_target = 0.0f;

//位置环相关变量
static positional_pid_params_t motor1_position_pid;
static positional_pid_params_t motor2_position_pid;

static int64_t motor1_position_target = 0;
static int64_t motor2_position_target = 0;

//寻红线环相关变量
positional_pid_params_t red_lines_pid;

// 位置环得出来的速度，这个值最后要给电机速度环
static float position_result_motor1_speed_target = 0.0f;
static float position_result_motor2_speed_target = 0.0f;
// 巡线环得出来的速度
__attribute__((used)) static float red_lines_result_motor1_speed_target = 0.0f;

// PID 管理之外的速度(想让小车以多少的速度前进)
float raw_target_speed = 0.0f;


void app_pid_run(void)
{
    static int16_t motor1_pwm_value = 0;
    static int16_t motor2_pwm_value = 0;

    static int32_t position1_measurement;
    static int32_t position2_measurement;
    switch (app_pid_status)
    {
    case APP_PID_INIT:
        // 速度环配置
        //  motor1 速度环 pid 参数初始化
        positional_pid_init(&motor1_speed_pid, 63, 25, 0, 0.0f, 1000, -1000);
        // 修改motor1 速度环 kp ki kd
        motor1_speed_pid.positional_pid_set_value(&motor1_speed_pid, 63, 25,
                                                  0,8.0f,800,-800);

        // motor2 速度环 pid 参数初始化
        positional_pid_init(&motor2_speed_pid, 63, 25, 0, 0.0f, 1000, -1000);
        // 修改motor2 速度环 kp ki kd
        motor1_speed_pid.positional_pid_set_value(&motor2_speed_pid, 63, 25,
                                                  0,8.0f,800,-800);

        //位置环配置
        // 位置环配置
        //  motor1 位置环 pid 参数初始化
        positional_pid_init(&motor1_position_pid, 0.0065f, 0, 0.01f, 10, 4, -4);
        // 修改 motor1 位置环 kp ki kd
        motor1_position_pid.positional_pid_set_value(&motor1_position_pid, 0.0065f, 0,
                                                     0.01f,5.0f,5,-5);
        // motor2 位置环 pid 参数初始化
        positional_pid_init(&motor2_position_pid, 0.0065f, 0, 0.01f, 10, 4, -4);
        // 修改 motor2 位置环 kp ki kd
        motor2_position_pid.positional_pid_set_value(&motor2_position_pid, 0.0065f, 0,
                                                     0.01f,5.0f,5,-5);
        app_pid_status++;
        break;
    case APP_PID_RUN:
        // 计算电机1的PWM输出值
        motor1_pwm_value = (int16_t)positional_pid_compute(
            &motor1_speed_pid, motor1_speed_target, get_m1_speed());
        // 应用电机1的PWM值
        motor1_pwm_value_set(motor1_pwm_value);

        // 计算电机2的pwm输出值
        motor2_pwm_value = (int16_t)positional_pid_compute(
            &motor2_speed_pid, motor2_speed_target, get_m2_speed());
        // 应用电机2的PWM值
        motor2_pwm_value_set(motor2_pwm_value);

#if DEBUG_PID_MOTOR1_SPEED == 1
        printf("m1 target,speed:%.3f,%.3f\n", motor1_speed_target, get_m1_speed());
#endif
#if DEBUG_PID_MOTOR2_SPEED == 1
        printf("m2 target,speed:%.3f,%.3f\n", motor2_speed_target, get_m2_speed());
#endif

        position1_measurement = get_m1_total_count() - 30000;
        position2_measurement = get_m2_total_count() - 30000;
        if (car_position_pid_enable_flag == 1)
        {
            position_result_motor1_speed_target = (positional_pid_compute(
                &motor1_position_pid, (float)motor1_position_target,
                (float)position1_measurement));
            position_result_motor2_speed_target = -(positional_pid_compute(
                &motor2_position_pid, (float)motor2_position_target,
                (float)position2_measurement));
        }
        else
        {
            motor1_position_target = position1_measurement;
            motor2_position_target = position2_measurement;
            position_result_motor1_speed_target = 0;
            position_result_motor2_speed_target = 0;
        }
#if DEBUG_PID_MOTOR1_POSITION == 1
        printf("m1 target,position:%ld,%d\n", (int32_t)motor1_position_target, position1_measurement);
#endif
#if DEBUG_PID_MOTOR2_POSITION == 1
        printf("m2 target,position:%d,%d\n", (int32_t)motor2_position_target, position2_measurement);
#endif

        motor1_speed_target =
            raw_target_speed + position_result_motor1_speed_target;
        motor2_speed_target =
            raw_target_speed + position_result_motor2_speed_target;

        break;
    case APP_PID_STOP:
        break;
    default:
        break;
    }
}
