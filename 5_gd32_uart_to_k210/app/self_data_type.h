#ifndef __SELF_DATA_TYPE_H__
#define __SELF_DATA_TYPE_H__
#include <stdio.h>

typedef union
{
    float array[4];
    struct
    {
        float q0;
        float q1;
        float q2;
        float q3;
    } quaternion;
} _fusion_data_quat_t;

typedef union
{
    float array[3];
    struct
    {
        float roll;
        float pitch;
        float yaw;
    } euler;
} _fusion_data_euler_t;

typedef struct
{
    _fusion_data_quat_t quaternion;
    _fusion_data_euler_t euler;
} fusion_data_t;

typedef struct
{
    volatile char work_mode;
    volatile char recognition; // 路口识别
    volatile int16_t top_block_offset;
    volatile int16_t center_block_offset;
    volatile int16_t left_block_offset;
    volatile int16_t right_block_offset;
    volatile char left_number;
    volatile char right_number;
} k210_data_t;

typedef struct
{
    // 小车病房号信息
    char need_to_medical_ward_number;
    char medical_ward_a;
    char medical_ward_b;
    char medical_ward_c;
    char medical_ward_d;
    char medical_ward_e;
    char medical_ward_f;
    char medical_ward_g;
    char medical_ward_h;
} medical_ward_info_t;

typedef struct
{
    // 小车1需要传输的信息
    char need_to_medical_ward_number;
    char medical_ward_a;
    char medical_ward_b;
    char medical_ward_c;
    char medical_ward_d;
    char medical_ward_e;
    char medical_ward_f;
    char medical_ward_g;
    char medical_ward_h;
    char car1_position; // 可选：a,b,c,d,e,f,g,h,0,1,2,3,4,5
    char car1_control_flag;
} car1_to_car2_info_t;

typedef struct
{
    // 小车2位置信息
    char car2_position; // 可选：a,b,c,d,e,f,g,h,0,1,2,3,4,5
} car2_to_car1_info_t;

#endif //__SELF_DATA_TYPE_H__
