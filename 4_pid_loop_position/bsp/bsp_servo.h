/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
 */
#ifndef __BSP_SERVO_H__
#define __BSP_SERVO_H__

/* defined the servo PWM timer -*/
#define SERVO_TIMER           TIMER7
#define SERVO_TIMER_CLK       RCU_TIMER7
/* defined the servo PWM1 pin: PC6 -*/
#define SERVO_1_GPIO_CLK      RCU_GPIOC
#define SERVO_1_GPIO_PORT     GPIOC
#define SERVO_1_GPIO_ALT_FUNC GPIO_AF_3
#define SERVO_1_GPIO_PUPD     GPIO_PUPD_PULLDOWN
#define SERVO_1_GPIO_PIN      GPIO_PIN_6
/* defined the servo PWM2 pin: PC7 */
#define SERVO_2_GPIO_CLK      RCU_GPIOC
#define SERVO_2_GPIO_PORT     GPIOC
#define SERVO_2_GPIO_ALT_FUNC GPIO_AF_3
#define SERVO_2_GPIO_PUPD     GPIO_PUPD_PULLDOWN
#define SERVO_2_GPIO_PIN      GPIO_PIN_7


void servo1_pwm_pulse_set(uint16_t pulse);
void servo2_pwm_pulse_set(uint16_t pulse);


#endif //__BSP_SERVO_H__
