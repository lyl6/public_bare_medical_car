/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
*/

//参考链接：https://blog.csdn.net/u014483560/article/details/128768907

#include <board.h>
#include "bsp_encoder.h"

#define _ENCODER_TIMER_RELOAD_VALUE 60000
														
encoder_state_t encoder_m1;
encoder_state_t encoder_m2;

static void encoder_gpio_init(void)
{
    // M1 encoder gpio init
    rcu_periph_clock_enable(M1_A_GPIO_CLK);
    rcu_periph_clock_enable(M1_B_GPIO_CLK);

    gpio_mode_set(M1_A_GPIO_PORT, GPIO_MODE_AF, M1_A_GPIO_PUPD, M1_A_GPIO_PIN);
    gpio_mode_set(M1_B_GPIO_PORT, GPIO_MODE_AF, M1_B_GPIO_PUPD, M1_B_GPIO_PIN);

    gpio_af_set(M1_A_GPIO_PORT, M1_A_GPIO_ALT_FUNC, M1_A_GPIO_PIN);
    gpio_af_set(M1_B_GPIO_PORT, M1_B_GPIO_ALT_FUNC, M1_B_GPIO_PIN);

    // M2 encoder gpio init
    rcu_periph_clock_enable(M2_A_GPIO_CLK);
    rcu_periph_clock_enable(M2_B_GPIO_CLK);

    gpio_mode_set(M2_A_GPIO_PORT, GPIO_MODE_AF, M2_A_GPIO_PUPD, M2_A_GPIO_PIN);
    gpio_mode_set(M2_B_GPIO_PORT, GPIO_MODE_AF, M2_B_GPIO_PUPD, M2_B_GPIO_PIN);

    gpio_af_set(M2_A_GPIO_PORT, M2_A_GPIO_ALT_FUNC, M2_A_GPIO_PIN);
    gpio_af_set(M2_B_GPIO_PORT, M2_B_GPIO_ALT_FUNC, M2_B_GPIO_PIN);

    return;
}
static void encoder_timer_init(void)
{
    // M1 Timer init
    timer_parameter_struct Timer_ParamType;
    timer_ic_parameter_struct Timer_IC_ParamType;

    rcu_periph_clock_enable(M1_TIMER_CLK);
    timer_deinit(M1_TIMER);
    timer_struct_para_init(&Timer_ParamType);

    Timer_ParamType.alignedmode = TIMER_COUNTER_EDGE;
    Timer_ParamType.clockdivision = TIMER_CKDIV_DIV1;
    Timer_ParamType.counterdirection = TIMER_COUNTER_UP;
    Timer_ParamType.period = _ENCODER_TIMER_RELOAD_VALUE;
    Timer_ParamType.prescaler = 0;
    Timer_ParamType.repetitioncounter = 0;
    timer_init(M1_TIMER, &Timer_ParamType);

    timer_channel_input_struct_para_init(&Timer_IC_ParamType);

    Timer_IC_ParamType.icfilter = 0x04;
    Timer_IC_ParamType.icpolarity = TIMER_IC_POLARITY_RISING;
    Timer_IC_ParamType.icselection = TIMER_IC_SELECTION_DIRECTTI;
    Timer_IC_ParamType.icprescaler = TIMER_IC_PSC_DIV1;
    timer_input_capture_config(M1_TIMER, M1_A_TIMER_CH, &Timer_IC_ParamType);
    timer_input_capture_config(M1_TIMER, M1_B_TIMER_CH, &Timer_IC_ParamType);

    timer_quadrature_decoder_mode_config(M1_TIMER, TIMER_QUAD_DECODER_MODE2,
                                         TIMER_IC_POLARITY_RISING,
                                         TIMER_IC_POLARITY_RISING);

    timer_interrupt_flag_clear(M1_TIMER, TIMER_INT_FLAG_UP);
		timer_interrupt_enable(M1_TIMER, TIMER_INT_UP);
    nvic_irq_enable(M1_TIMER_IRQn, 0, 0);
    timer_enable(M1_TIMER);

    // M2 Timer init
    rcu_periph_clock_enable(M2_TIMER_CLK);
    timer_deinit(M2_TIMER);
    timer_struct_para_init(&Timer_ParamType);

    Timer_ParamType.alignedmode = TIMER_COUNTER_EDGE;
    Timer_ParamType.clockdivision = TIMER_CKDIV_DIV1;
    Timer_ParamType.counterdirection = TIMER_COUNTER_UP;
    Timer_ParamType.period = _ENCODER_TIMER_RELOAD_VALUE;
    Timer_ParamType.prescaler = 0;
    Timer_ParamType.repetitioncounter = 0;
    timer_init(M2_TIMER, &Timer_ParamType);

    timer_channel_input_struct_para_init(&Timer_IC_ParamType);

    Timer_IC_ParamType.icfilter = 0x04;
    Timer_IC_ParamType.icpolarity = TIMER_IC_POLARITY_RISING;
    Timer_IC_ParamType.icselection = TIMER_IC_SELECTION_DIRECTTI;
    Timer_IC_ParamType.icprescaler = TIMER_IC_PSC_DIV1;
    timer_input_capture_config(M2_TIMER, M2_A_TIMER_CH, &Timer_IC_ParamType);
    timer_input_capture_config(M2_TIMER, M2_B_TIMER_CH, &Timer_IC_ParamType);

    timer_quadrature_decoder_mode_config(M2_TIMER, TIMER_QUAD_DECODER_MODE2,
                                         TIMER_IC_POLARITY_RISING,
                                         TIMER_IC_POLARITY_RISING);
    timer_interrupt_flag_clear(M2_TIMER, TIMER_INT_FLAG_UP);
		timer_interrupt_enable(M2_TIMER, TIMER_INT_UP);
    nvic_irq_enable(M2_TIMER_IRQn, 0, 0);
    timer_enable(M2_TIMER);

    timer_counter_value_config(M1_TIMER, _ENCODER_TIMER_RELOAD_VALUE/2);
    timer_counter_value_config(M2_TIMER, _ENCODER_TIMER_RELOAD_VALUE/2);

    return;
}

static void encoder_count_timer_init(void)
{
    timer_parameter_struct Timer_ParamType;
    rcu_periph_clock_enable(ENCODER_COUNT_TIMER_CLK);
    timer_deinit(ENCODER_COUNT_TIMER);
    timer_struct_para_init(&Timer_ParamType);

    Timer_ParamType.alignedmode = TIMER_COUNTER_EDGE;
    Timer_ParamType.clockdivision = TIMER_CKDIV_DIV1;
    Timer_ParamType.counterdirection = TIMER_COUNTER_UP;
//    Timer_ParamType.period = 100-1;  //100KHZ
//    Timer_ParamType.prescaler = 12-1;  //120Mhz
    Timer_ParamType.period = 200 - 1;   // 20ms
    Timer_ParamType.prescaler = 12000 - 1; // 120Mhz
    Timer_ParamType.repetitioncounter = 0;
    timer_init(ENCODER_COUNT_TIMER, &Timer_ParamType);
    timer_interrupt_enable(ENCODER_COUNT_TIMER, TIMER_INT_UP);
    timer_interrupt_flag_clear(ENCODER_COUNT_TIMER,TIMER_INT_FLAG_UP);
    nvic_irq_enable(ENCODER_COUNT_TIMER_IRQn, 1, 0);
    timer_enable(ENCODER_COUNT_TIMER);

    return;
}

void M1_TIMER_IRQHANDLER(void)
{
    if (SET == timer_interrupt_flag_get(M1_TIMER, TIMER_INT_FLAG_UP))
    {
        if (timer_counter_read(M1_TIMER) < _ENCODER_TIMER_RELOAD_VALUE/2)
        {
            encoder_m1.overflow_count++;
        }
        else if (timer_counter_read(M1_TIMER) >= _ENCODER_TIMER_RELOAD_VALUE/2)
        {
            encoder_m1.overflow_count--;
        }
        timer_counter_value_config(M1_TIMER, _ENCODER_TIMER_RELOAD_VALUE/2);
        timer_interrupt_flag_clear(M1_TIMER, TIMER_INT_FLAG_UP);
    }
}

void M2_TIMER_IRQHANDLER(void)
{
    if (SET == timer_interrupt_flag_get(M2_TIMER, TIMER_INT_FLAG_UP))
    {
        if (timer_counter_read(M2_TIMER) < _ENCODER_TIMER_RELOAD_VALUE/2)
        {
            encoder_m2.overflow_count++;
        }
        else if (timer_counter_read(M2_TIMER) >= _ENCODER_TIMER_RELOAD_VALUE/2)
        {
            encoder_m2.overflow_count--;
        }
        timer_counter_value_config(M2_TIMER, _ENCODER_TIMER_RELOAD_VALUE/2);
        timer_interrupt_flag_clear(M2_TIMER, TIMER_INT_FLAG_UP);
    }
}

void ENCODER_COUNT_TIMER_IRQHANDLER(void)
{
    static uint64_t count = 0;
    if (SET == timer_interrupt_flag_get(ENCODER_COUNT_TIMER, TIMER_INT_FLAG_UP))
    {
        count++;
        // m1
        encoder_m1.direction = 0;

        encoder_m1.total_count =
            timer_counter_read(M1_TIMER) + encoder_m1.overflow_count * _ENCODER_TIMER_RELOAD_VALUE/2;

        // 单位是圈/秒
        //(编码器差值/(四倍频*编码器单圈脉冲*电机减速比))*测速频率（50Hz）
        encoder_m1.speed =
            (float)(encoder_m1.total_count - encoder_m1.last_count)
            / (4 * MOTOR_PULSE_PER_REVOLUTION * MOTOR_REDUCTION_RATIO) * 50;
        encoder_m1.last_count = encoder_m1.total_count;
        // m2
        encoder_m2.direction = 0;

        encoder_m2.total_count =
            timer_counter_read(M2_TIMER) + encoder_m2.overflow_count * _ENCODER_TIMER_RELOAD_VALUE/2;

        // 单位是圈/秒
        //(编码器差值/(四倍频*编码器单圈脉冲*电机减速比))*测速频率（50Hz）
        encoder_m2.speed =
            (float)(encoder_m2.total_count - encoder_m2.last_count)
            / (4 * MOTOR_PULSE_PER_REVOLUTION * MOTOR_REDUCTION_RATIO) * 50;
        //两个对向的轮子转动方向要和车运动方向保持一致，这里将电机2速度值取反
        encoder_m2.speed = -encoder_m2.speed;

        encoder_m2.last_count = encoder_m2.total_count;

        timer_interrupt_flag_clear(ENCODER_COUNT_TIMER, TIMER_INT_FLAG_UP);
    }

}

static void set_encoder_struct_to_default(encoder_state_t *encoder)
{
    encoder->total_count = 0;
    encoder->last_count = _ENCODER_TIMER_RELOAD_VALUE/2;
    encoder->overflow_count = 0;
    encoder->direction = 0;
    encoder->speed = 0.0f;
}

float get_car_distance(void)
{
    static float car_distance = 0;
    car_distance =
        ((encoder_m1.total_count - _ENCODER_TIMER_RELOAD_VALUE / 2)/(4*MOTOR_PULSE_PER_REVOLUTION*MOTOR_REDUCTION_RATIO))*MOTOR_WHEEL_CIRCUMFERENCE;
    return car_distance;
}

float get_m1_speed(void) { return encoder_m1.speed; }
float get_m2_speed(void) { return encoder_m2.speed; }

uint32_t get_m1_total_count(void) { return encoder_m1.total_count; }
uint32_t get_m2_total_count(void) { return encoder_m2.total_count; }

void bsp_encoder_init(void)
{
    encoder_gpio_init();
    encoder_count_timer_init();
    encoder_timer_init();
    set_encoder_struct_to_default(&encoder_m1);
    set_encoder_struct_to_default(&encoder_m2);
    return;
}

