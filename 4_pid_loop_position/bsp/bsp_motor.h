/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
 */
#ifndef __BSP_MOTOR_H__
#define __BSP_MOTOR_H__

/* defined the M1 PWM timer -*/
#define M1_MOTOR_TIMER       TIMER8
#define M1_MOTOR_TIMER_CLK   RCU_TIMER8
/* defined the M1 PWM IN1 pin: PA2 -*/
#define M1_IN1_GPIO_CLK      RCU_GPIOA
#define M1_IN1_GPIO_PORT     GPIOA
#define M1_IN1_GPIO_ALT_FUNC GPIO_AF_3
#define M1_IN1_GPIO_PUPD     GPIO_PUPD_NONE
#define M1_IN1_GPIO_PIN      GPIO_PIN_2
/* defined the M1 PWM IN2 pin: PA3 */
#define M1_IN2_GPIO_CLK      RCU_GPIOA
#define M1_IN2_GPIO_PORT     GPIOA
#define M1_IN2_GPIO_ALT_FUNC GPIO_AF_3
#define M1_IN2_GPIO_PUPD     GPIO_PUPD_NONE
#define M1_IN2_GPIO_PIN      GPIO_PIN_3

/* defined the M2 PWM timer -*/
#define M2_MOTOR_TIMER       TIMER11
#define M2_MOTOR_TIMER_CLK   RCU_TIMER11
/* defined the M2 PWM IN1 pin: PB14 -*/
#define M2_IN1_GPIO_CLK      RCU_GPIOB
#define M2_IN1_GPIO_PORT     GPIOB
#define M2_IN1_GPIO_ALT_FUNC GPIO_AF_9
#define M2_IN1_GPIO_PUPD     GPIO_PUPD_NONE
#define M2_IN1_GPIO_PIN      GPIO_PIN_14
/* defined the M2 PWM IN2 pin: PB15 */
#define M2_IN2_GPIO_CLK      RCU_GPIOB
#define M2_IN2_GPIO_PORT     GPIOB
#define M2_IN2_GPIO_ALT_FUNC GPIO_AF_9
#define M2_IN2_GPIO_PUPD     GPIO_PUPD_NONE
#define M2_IN2_GPIO_PIN      GPIO_PIN_15

void bsp_motor_init(void);
void motor1_pwm_value_set(int16_t value);
void motor2_pwm_value_set(int16_t value);

#endif //__BSP_MOTOR_H__
