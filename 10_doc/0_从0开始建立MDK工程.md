# 提前准备

参考立创梁山派[入门教程资料](https://lceda001.feishu.cn/wiki/Yl6mwgNQiiwolrkqBbscxk45nFb)中的开发环境介绍和工程模板创建，下载并安装最新的KEIL（MDK）软件和[GD32官方的器件支持包](https://www.keil.arm.com/packs/gd32f4xx_dfp-gigadevice/devices/)（推荐用离线安装的方式）。

**需要注意**：最新安装的MDK5（KEIL）已经不自带AC5编译器了，如果需要用到AC5编译器的话需要自行下载安装，打开这个[链接](https://developer.arm.com/documentation/ka005184/latest)，点击[latest release of Arm Compiler for Embedded](https://developer.arm.com/downloads/view/ACOMPE)，登录之后（国内手机号也可以直接登录）下载最新的AC5，`Arm Compiler 5.06 update 7 (build 960) Win32`,注意第一个是linux用的，第二个才是Windows用的。我们这里要下载第二个。解压之后点`setup.exe`安装到你想要安装的位置就可以了。记住这个位置，随便打开一个工程，`Project->Manage->Project Items->Folders/Extensions` 里面的`Setup Default ARM Compiler Version`。设置你之前安装的AC5 路径就可以了。

AC5目前已经停止开发维护和支持了（也就是说后面发现什么BUG官方也不会修了），AC6的编译速度更快，编译出来的固件优化更好，固件尺寸更小，能支持更好的新C/C++语法。但是当前很多库还没有完全支持AC6，建议新的工程都用AC6来编译。

# 建立裸机工程模板

## 1_创建对应文件夹

![image-20230803101424995](./images/image-20230803101424995.png)

> ├─app
> ├─bsp
> ├─board
> ├─libraries
> ├─module
> └─project
>
> * app：存放应用程序，由用户编写
> * bsp：存放和底层相关的支持包
> * board：存放板子初始化文件和链接脚本
> * libraries：存放各种库文件，CMSIS，芯片固件库，文件系统库，网络库等
> * module：主要存放各种软件模块，比如软件定时器，PID,FIFO,状态机等
> * project：存放工程文件（目前只支持MDK5）

## 2_获取GD32标准库源文件

打开[链接](https://www.gd32mcu.com/cn/download/7?kw=GD32F4)，下载最新的[GD32F4xx Firmware Library](https://www.gd32mcu.com/download/down/document_id/247/path_type/1)，我这里下载的是V3.0.4，发布时间为2023-03-07。

打开下载的GD32F4xx标准固件库，里面的目录如下图示。

![image-20230823152732155](./images/image-20230823152732155.png)

- Docs：文档文件夹包含的是官方开发板的原理图和固件库使用参考。
- Examples：示例文件夹包含的是官方编写的示例代码，涉及芯片的大部分功能。
- Firmware：固件文件夹里面有3个文件夹，包含CMSIS，标准外设库和USB库，存放官方封装的一些库函数，方便用户开发使用。
- Template：模板文件夹是工程模板文件夹，里面包含IAR和Keil的工程示例。
- Utilities：公共文件夹包含一些第三方组件和GD32配套的开发板文件。

将其解压后的Firmware文件夹里面的内容放入上面建立的libraries文件夹里面：

![image-20230803101938182](./images/image-20230803101938182.png)

## 3_新建MDK工程

在上面建立的`project`文件夹里面创建一个`MDK(V5)`文件夹用来存放`keil MDK ARM V5`版本的工程文件。 打开`Keil uVision5`创建一个新的工程： 单击->`Project`->`New uVision Project` ->选好芯片型号=`GD32F470ZG`。

![new_mdk_project](./images/new_mdk_project.gif)

右键Target->单击Manage Project Items 如下所示设计分组（双击可以修改名字）：

![image-20230803113631397](./images/image-20230803113631397.png)

![add_mdk_group](./images/add_mdk_group.gif)

将`\libraries\CMSIS\GD\GD32F4xx\Source\ARM`目录下的`startup_gd32f450_470.s`（启动文件）添加到STARTUP组里（注意最后要点击对话框左下角的`OK`按钮才能生效）

启动文件使用汇编写的，他是上电或复位后执行的第一段代码：

1. 设置堆栈大小和PC指针。
2. 配置中断向量表：设置中断向量表的位置，这样当中断发生时，CPU能够找到相应的中断服务程序。
3. 定义中断服务例程的默认处理函数：通常会定义一个默认的中断处理函数，当未定义特定的中断处理函数时，会调用这个默认函数。在某些启动文件中，都是了弱定义的中断处理函数，允许我们在其C代码中重定义。
4. 系统时钟初始化。
5. 跳转到c语言的main函数里面。

![add_mdk_startup](./images/add_mdk_startup.gif)

将`\libraries\CMSIS\GD\GD32F4xx\Source\`目录下的system_gd32f4xx.c添加到CMSIS组里面：

![image-20230803115007389](./images/image-20230803115007389.png)

将`\libraries\GD32F4xx_standard_peripheral\Source`目录下的所有文件添加到STD_Driver组里面：

![image-20230803115401195](./images/image-20230803115401195.png)

在`board`文件夹下新建`board.c`和`board.h`文件：

board.c:

```c
#include <stdint.h>
#include <board.h>


//全局变量，系统时间每过1ms这个数就自加1.
static __IO uint32_t g_system_tick = 0;

//下面的都是各种错误中断，函数里还没有内容，后面实现串口打印之后可以在函数中添加打印信息，方便我们找问题。
//不可屏蔽中断
/*!
    \brief      this function handles NMI exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void NMI_Handler(void)
{
}
//硬件错误中断
/*!
    \brief      this function handles HardFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void HardFault_Handler(void)
{
    /* if Hard Fault exception occurs, go to infinite loop */
    while(1) {
    }
}
//存储器故障中断
/*!
    \brief      this function handles MemManage exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void MemManage_Handler(void)
{
    /* if Memory Manage exception occurs, go to infinite loop */
    while(1) {
    }
}
//总线错误中断
/*!
    \brief      this function handles BusFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void BusFault_Handler(void)
{
    /* if Bus Fault exception occurs, go to infinite loop */
    while(1) {
    }
}
//指令错误中断
/*!
    \brief      this function handles UsageFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void UsageFault_Handler(void)
{
    /* if Usage Fault exception occurs, go to infinite loop */
    while(1) {
    }
}
//错误中断，一般是没有正常进入main函数，尤其是串口的半主机问题。
/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler */
    /* User can add his own implementation to report the HAL error return state */
    while (1)
    {
    }
    /* USER CODE END Error_Handler */
}
//时钟配置，初始化系统时钟，让systick中断每1ms执行一次。
/*!
    \brief      configure systick
    \param[in]  none
    \param[out] none
    \retval     none
*/
void systick_config(void)
{
    /* setup systick timer for 1000Hz interrupts */
    if(SysTick_Config(SystemCoreClock / 1000U)) {
        /* capture error */
        while(1) {
        }
    }
    /* configure the systick handler priority */
    NVIC_SetPriority(SysTick_IRQn, 0x00U);
}
//每来一次systick中断就把下面这个变量加1
/*!
    \brief      this function handles SysTick exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void SysTick_Handler(void)
{
	g_system_tick ++;
}
//获取全局变量系统计数的值
uint32_t get_system_tick(void)
{
    return g_system_tick;
}
//重定向中断向量表（向量表就是一个大数组，存储了当发生对应中断时应该调用的函数地址），当中断或异常发生时，处理器会查找向量表以找到相应的处理函数。这个函数中的代码设置了向量表的基址。这个和后面做BootLoader升级以及直接将代码烧录到单片机内存中有关。这个初学不需要管，就当他是空气吧。
//接下来就是调用上面的时钟初始化函数了，这个函数在main函数中被调用。
/**
 * This function will initial GD32 board.
 */
void board_init(void)
{
    /* NVIC Configuration */
#define NVIC_VTOR_MASK              0x3FFFFF80
#ifdef  VECT_TAB_RAM
    /* Set the Vector Table base location at 0x10000000 */
    SCB->VTOR  = (0x10000000 & NVIC_VTOR_MASK);
#else  /* VECT_TAB_FLASH  */
    /* Set the Vector Table base location at 0x08000000 */
    SCB->VTOR  = (0x08000000 & NVIC_VTOR_MASK);
#endif

    systick_config();

}

/**
 -  @brief  用内核的 systick 实现的微秒延时
 -  @note   None
 -  @param  _us:要延时的us数
 -  @retval None
*/
void delay_us(uint32_t _us)
{
    uint32_t ticks;
    uint32_t told, tnow, tcnt = 0;

    // 计算需要的时钟数 = 延迟微秒数 * 每微秒的时钟数
    // 后面的80是补偿值，因为语句运行也需要时间。
    // 补偿值是GD32F470在240Mhz运行条件下用逻辑分析仪测出来的。
    ticks = _us * (SystemCoreClock / 1000000) - 80;

    // 获取当前的SysTick值
    told = SysTick->VAL;

    while (1)
    {
        // 重复刷新获取当前的SysTick值
        tnow = SysTick->VAL;

        if (tnow != told)
        {
            if (tnow < told)
                tcnt += told - tnow;
            else
                tcnt += SysTick->LOAD - tnow + told;

            told = tnow;

            // 如果达到了需要的时钟数，就退出循环
            if (tcnt >= ticks)
                break;
        }
    }
}


```

来解释一下上面的微秒延时，不要在用__nop指令（无操作指令）或者for循环的自加自减指令了，它们根本不准：

* 几乎不能移植，系统时钟变了他就不对了。
* 不精确，编译器一优化时间就错了。

上面的微秒延时是用内核的systick定时器来实现的。

1. **输入参数**：函数接受一个参数 `_us`，表示要延迟的微秒数，最大是4294967295，也就是最多可以定时4000多秒，大概七十多分钟。注意这个函数可不能乱用，你延时的这么久的时间单片机一直在这个while循环里面出不去，在做无用功。
2. **计算时钟数**：首先，函数计算了需要的时钟数（`ticks`）。是通过将延迟的微秒数乘以每微秒的时钟数来实现的。`SystemCoreClock` 是系统核心时钟频率，以赫兹为单位。最后，从结果中减去80，这是一个补偿值，因为执行语句本身也需要时间，不要也可以，差距其实很小的。这个值要以实际测量为准。
3. **获取当前SysTick值**：SysTick是ARM Cortex-M内核的一个组件，用于产生一个固定的时间间隔的中断。在这个函数中，SysTick被用作一个计时器。首先，通过读取`SysTick->VAL`来获取当前SysTick的值，并将其存储在`told`变量中。
4. **循环计时**：函数进入一个无限循环，不断读取当前的SysTick值（`tnow`）。如果`tnow`不等于`told`，则计算两者之间的差异，并累加到`tcnt`变量中。如果`tnow`小于`told`，则直接计算差值；否则，需要加上SysTick的加载值（`SysTick->LOAD`）来计算差值。因为SysTick计数器是向下计数的，当它到达0时会自动重载。
5. **检查是否达到延迟时间**：如果累计的时钟数（`tcnt`）达到或超过所需的时钟数（`ticks`），则退出循环。
6. **延迟完成**：当循环结束时，函数返回，延迟完成。

board.h

```c
#ifndef __BOARD_H__
#define __BOARD_H__

#include "gd32f4xx.h"
#include "stdint.h"
#include "stdio.h"
#include "gd32f4xx_libopt.h"
#include "gd32f4xx_exti.h"


void board_init(void);
uint32_t get_system_tick(void);
void delay_us(uint32_t _us);

#endif

```

同样的，把他添加到Board分组中：

![image-20230803120305642](./images/image-20230803120305642.png)

加入头文件路径：

![add_mdk_include](./images/add_mdk_include.gif)

继续补全头文件路径如下图所示：

![image-20230803120908540](./images/image-20230803120908540.png)

将默认编译器改为V5：

![modify_mdk_ac](./images/modify_mdk_ac.gif)

添加全局宏定义`USE_STDPERIPH_DRIVER,GD32F470`，并勾选C99 Mode，点击OK保存设置：

![modify_mdk_define](./images/modify_mdk_define.gif)

## 4_添加主代码并修改时钟验证

立创梁山派是板载了一个25M的高精度外部晶振的，而GD32的固件库默认是用内部精度不高的高速晶振，将`system_gd32f4xx.c`文件中的

```c
#define __SYSTEM_CLOCK_240M_PLL_IRC16M          (uint32_t)(240000000)
//#define __SYSTEM_CLOCK_240M_PLL_8M_HXTAL        (uint32_t)(240000000)
//#define __SYSTEM_CLOCK_240M_PLL_25M_HXTAL       (uint32_t)(240000000)
```

将上面的`__SYSTEM_CLOCK_240M_PLL_IRC16M`注释掉，把`__SYSTEM_CLOCK_240M_PLL_25M_HXTAL`解除注释，改成下面这样：

```c
//#define __SYSTEM_CLOCK_240M_PLL_IRC16M          (uint32_t)(240000000)
//#define __SYSTEM_CLOCK_240M_PLL_8M_HXTAL        (uint32_t)(240000000)
#define __SYSTEM_CLOCK_240M_PLL_25M_HXTAL       (uint32_t)(240000000)
```

在app目录中新建如下所示的`main.c`文件：

```c
#include "board.h"


#define RCU_LED2  RCU_GPIOD
#define PORT_LED2 GPIOD
#define PIN_LED2 GPIO_PIN_7


void led_gpio_config(void)
{
	rcu_periph_clock_enable(RCU_LED2);
	gpio_mode_set(PORT_LED2,GPIO_MODE_OUTPUT,GPIO_PUPD_NONE,PIN_LED2);
	gpio_output_options_set(PORT_LED2,GPIO_OTYPE_PP,GPIO_OSPEED_50MHZ,PIN_LED2);
}
/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    board_init();
	led_gpio_config();
    while (1)
    {
		gpio_bit_write(PORT_LED2,PIN_LED2,SET);
		delay_us(1000000);
		gpio_bit_write(PORT_LED2,PIN_LED2,RESET);
		delay_us(1000000);
    }
}

```

将`main.c`文件内容替换为上面这个。
```c
/*!
    \file    gd32f4xx_libopt.h
    \brief   library optional for gd32f4xx

    \version 2016-08-15, V1.0.0, firmware for GD32F4xx
    \version 2018-12-12, V2.0.0, firmware for GD32F4xx
    \version 2020-09-30, V2.1.0, firmware for GD32F4xx
    \version 2022-03-09, V3.0.0, firmware for GD32F4xx
*/

/*
    Copyright (c) 2022, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software without
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

#ifndef GD32F4XX_LIBOPT_H
#define GD32F4XX_LIBOPT_H

#if defined (GD32F450) || defined (GD32F405) || defined (GD32F407) || defined (GD32F470) || defined (GD32F425) || defined (GD32F427)
    #include "gd32f4xx_rcu.h"
    #include "gd32f4xx_adc.h"
    #include "gd32f4xx_can.h"
    #include "gd32f4xx_crc.h"
    #include "gd32f4xx_ctc.h"
    #include "gd32f4xx_dac.h"
    #include "gd32f4xx_dbg.h"
    #include "gd32f4xx_dci.h"
    #include "gd32f4xx_dma.h"
    #include "gd32f4xx_exti.h"
    #include "gd32f4xx_fmc.h"
    #include "gd32f4xx_fwdgt.h"
    #include "gd32f4xx_gpio.h"
    #include "gd32f4xx_syscfg.h"
    #include "gd32f4xx_i2c.h"
    #include "gd32f4xx_iref.h"
    #include "gd32f4xx_pmu.h"
    #include "gd32f4xx_rtc.h"
    #include "gd32f4xx_sdio.h"
    #include "gd32f4xx_spi.h"
    #include "gd32f4xx_timer.h"
    #include "gd32f4xx_trng.h"
    #include "gd32f4xx_usart.h"
    #include "gd32f4xx_wwdgt.h"
    #include "gd32f4xx_misc.h"
#endif

#if defined (GD32F450) || defined (GD32F470)
    #include "gd32f4xx_enet.h"
    #include "gd32f4xx_exmc.h"
    #include "gd32f4xx_ipa.h"
    #include "gd32f4xx_tli.h"
#endif

#if defined (GD32F407) || defined (GD32F427)
    #include "gd32f4xx_enet.h"
    #include "gd32f4xx_exmc.h"
#endif

#endif /* GD32F4XX_LIBOPT_H */

```
在board文件夹下新建gd32f4xx_libopt.h文件，把上面的内容复制到文件里面，看一下编译烧录是否正常。


全编译烧录之后，没有错误和警告，按下立创梁山派的复位按键后，LED2灯开始时闪烁就说明工程模板建立成功了。


# 编译烧录验证

点击Rebuild按钮编译全工程，确保下方是` 0 Error(s), 0 Warning(s)`

![modify_mdk_rebuild_download](./images/modify_mdk_rebuild_download.gif)

选择你当前所使用的下载器（我这里用的立创梁山派自带的下载器，属于DAP下载器）,你也可以用，连接好立创梁山派的供电和下载线，点击下载按钮，成功后按下复位键，如果立创梁山派的LED2开始间隔一秒亮一次，说明模板工程搭建OK了。