#include "board.h"
#include "ringbuffer.h"


/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    board_init();
    bsp_led_init();
    bsp_uart_init();
    buzzer_init();

    // 初始化四个软件定时器
    // 设置SOFT_TIMER_0的超时时间为100ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_0, 100);
    // 设置SOFT_TIMER_1的超时时间为200ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_1, 200);
    // 设置SOFT_TIMER_3的超时时间为300ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_2, 300);
    // 设置SOFT_TIMER_4的超时时间为4000ms，重复定时器
    soft_timer_repeat_init(SOFT_TIMER_3, 400);

    while (1)
    {
        // 检查SOFT_TIMER_0是否超时
        if (soft_timer_is_timeout(SOFT_TIMER_0))
        {
            bsp_led_toggle(LED1);
        }
        // 检查SOFT_TIMER_1是否超时
        if (soft_timer_is_timeout(SOFT_TIMER_1))
        {
            bsp_led_toggle(LED2);
        }
        // 检查SOFT_TIMER_3是否超时
        if (soft_timer_is_timeout(SOFT_TIMER_2))
        {
            bsp_led_toggle(LED3);
        }
        // 检查SOFT_TIMER_4是否超时
        if (soft_timer_is_timeout(SOFT_TIMER_3))
        {
            bsp_led_toggle(LED4);
        }
    }
}
