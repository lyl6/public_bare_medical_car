# 裸机版基于立创梁山派的21年电赛F题智能送药小车

> **之前[基于RT-Thread制作的21年电赛送药小车](https://gitee.com/lcsc/medical_car)许多人反映太难了，现在再用裸机分步骤实现一遍，并进一步补充文档资料，更新视频来方便大家理解。**

## 裸机版软件文档如下：

1. [0_从0开始建立MDK工程](./10_doc/0_从0开始建立MDK工程.md)
2. [1_底层驱动介绍](./10_doc/1_底层驱动介绍.md)
3. [2_软件模块介绍](./10_doc/2_软件模块介绍.md)
4. [3_让电机和舵机动起来](./10_doc/3_让电机和舵机动起来.md)
5. [4_获取电机转速](./10_doc/4_获取电机转速.md)
6. **[5_电机的PID速度环与位置环调试](./10_doc/5_电机的PID速度环与位置环调试.md)**
7. [6_K210更换固件及进行颜色识别](./10_doc/6_K210更换固件及进行颜色识别.md)
8. [7_K210的KPU数字识别训练](./10_doc/7_K210的KPU数字识别训练.md)
9. [8_K210实际代码讲解](./10_doc/8_K210实际代码讲解.md)
10. [9_立创梁山派与K210串口通讯协议框架搭建](./10_doc/9_立创梁山派与K210串口通讯协议框架搭建.md)
11. [10_小车巡红线环的调试与实现](./10_doc/10_小车巡红线环的调试与实现.md)
12. [11_药物检测与指示灯控制](./10_doc/11_药物检测与指示灯控制.md)
13. [12_送药小车基础部分](./10_doc/12_送药小车基础部分.md)
14. [13_送药小车发挥部分](./10_doc/13_送药小车发挥部分.md)

## 裸机版视频教程如下：

1. [0_从0开始建立MDK工程](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=1)
2. [1_底层驱动介绍](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=2)
3. [2_软件模块介绍](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=3)
4. [3_让电机和舵机动起来](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=4)
5. [4_获取电机转速](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=5)
6. [**5_电机的PID速度环与位置环调试**](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=6)
7. [6_K210更换固件及进行颜色识别](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=7)
8. [7_K210的KPU数字识别训练](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=8)
9. [8_K210实际代码讲解](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=9)
10. [9_立创梁山派与K210串口通讯协议框架搭建](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=10)
11. [10_小车巡红线环的调试与实现](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=11)
12. [11_药物检测与指示灯控制](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=12)
13. [12_送药小车基础部分](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=13)
14. [13_送药小车发挥部分](https://www.bilibili.com/video/BV1Pb4y1V7XF?p=14)

# 工程目录说明

* 0_template：模板工程文件，编译下载之后四个LED灯应该以不同频率闪烁。
* 1_turn_the_motor：让电机动起来，编译下载之后两个电机应该会一起转起来。
* 2_get_motor_speed：获取电机速度，由`printf`打印出轮子的当前转速，可以打开串口（921600-8-1-N）查看。
* 3_pid_loop_speed：PID速度环，让电机以确定的转速运动
* 4_pid_loop_position: PID位置环，让电路转动指定的距离
* 5_gd32_uart_to_k210: 立创梁山派和K210建立串口通讯
* 6_k210_code_and_picture: K210进行拍照图像采集的代码以及实际运行主代码
* 7_pid_loop_find_red_lines:PID巡线环，让小车按照地上的红线前进。
* 8_car_basic_requir:21年电赛送药小车基础要求
* 9_car_advance: 21年电赛送药小车发挥部分
* 10_doc：相关文档资料

