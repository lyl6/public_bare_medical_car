/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-26     LCKFB-yzh    first version
 */
#ifndef _APP_PID_H
#define _APP_PID_H

#include "stdint.h"

// 激活小车两个电机位置PID
void active_car_position_pid(void);
// 停用小车两个电机位置PID
void deactive_car_position_pid(void);
// 激活小车寻红线PID
void active_car_red_lines_pid(void);
// 停用小车寻红线PID
void deactive_car_red_lines_pid(void);

void set_car_position_max_speed(float _speed);

void car_go_position(int32_t _position_target);
void car_spin_position(int32_t _position_target);

// 设置巡线速度
void set_car_red_lines_speed(float _speed);

int64_t get_m1_position_target(void);
int64_t get_m1_position_measurement(void);

#endif
