/*
* 梁山派软硬件资料与相关扩展板软硬件资料官网全部开源
* 开发板官网：www.lckfb.com
* 技术支持常驻论坛，任何技术问题欢迎随时交流学习
* 立创论坛：club.szlcsc.com
* 关注bilibili账号：【立创开发板】，掌握我们的最新动态！
* 不靠卖板赚钱，以培养中国工程师为己任
* Change Logs:
* Date           Author       Notes
* 2023-07-21     LCKFB-yzh    first version
 */
#include <board.h>
#include <bsp_motor.h>

/**
 -  @brief  电机驱动的PWM控制引脚初始化
 -  @note   None
 -  @param  None
 -  @retval None
   */
static void motor_pwm_gpio_init(void)
{
    // motor1 pwm gpio init
    rcu_periph_clock_enable(M1_IN1_GPIO_CLK);
    rcu_periph_clock_enable(M1_IN2_GPIO_CLK);

    gpio_mode_set(M1_IN1_GPIO_PORT, GPIO_MODE_AF, M1_IN1_GPIO_PUPD,
                  M1_IN1_GPIO_PIN);
    gpio_mode_set(M1_IN2_GPIO_PORT, GPIO_MODE_AF, M1_IN2_GPIO_PUPD,
                  M1_IN2_GPIO_PIN);

    gpio_af_set(M1_IN1_GPIO_PORT, M1_IN1_GPIO_ALT_FUNC, M1_IN1_GPIO_PIN);
    gpio_af_set(M1_IN2_GPIO_PORT, M1_IN2_GPIO_ALT_FUNC, M1_IN2_GPIO_PIN);

    // motor2 pwm gpio init
    rcu_periph_clock_enable(M2_IN1_GPIO_CLK);
    rcu_periph_clock_enable(M2_IN2_GPIO_CLK);

    gpio_mode_set(M2_IN1_GPIO_PORT, GPIO_MODE_AF, M2_IN1_GPIO_PUPD,
                  M2_IN1_GPIO_PIN);
    gpio_mode_set(M2_IN2_GPIO_PORT, GPIO_MODE_AF, M2_IN2_GPIO_PUPD,
                  M2_IN2_GPIO_PIN);

    gpio_af_set(M2_IN1_GPIO_PORT, M2_IN1_GPIO_ALT_FUNC, M2_IN1_GPIO_PIN);
    gpio_af_set(M2_IN2_GPIO_PORT, M2_IN2_GPIO_ALT_FUNC, M2_IN2_GPIO_PIN);

    return;
}

/**
 -  @brief  电机驱动的硬件定时器初始化
 -  @note   该函数将两个电机定时器的频率设置为60KHz，该项目所用到的电机驱动芯片AT8870最高可以接收100KHz的频率。
 -  @param  None
 -  @retval None
   */
static void motor_timer_init(void)
{
    // motor1 pwm timer init
    timer_parameter_struct timer_param_type;
    timer_oc_parameter_struct timer_oc_init_struct;

    rcu_periph_clock_enable(M1_MOTOR_TIMER_CLK);
    timer_deinit(M1_MOTOR_TIMER);
    timer_struct_para_init(&timer_param_type);

    timer_param_type.alignedmode = TIMER_COUNTER_EDGE;
    timer_param_type.clockdivision = TIMER_CKDIV_DIV1;
    timer_param_type.counterdirection = TIMER_COUNTER_UP;
    timer_param_type.period = 1000-1;   //60kHz
    timer_param_type.prescaler = 4-1;   //240Mhz/4=60MHz
    timer_param_type.repetitioncounter = 0;
    timer_init(M1_MOTOR_TIMER, &timer_param_type);

    timer_channel_output_struct_para_init(&timer_oc_init_struct);
    timer_oc_init_struct.ocpolarity = TIMER_OC_POLARITY_HIGH;
    timer_oc_init_struct.outputstate = TIMER_CCX_ENABLE;
    timer_oc_init_struct.outputnstate = TIMER_CCXN_DISABLE;
    timer_oc_init_struct.ocnpolarity = TIMER_OCN_POLARITY_HIGH;
    timer_oc_init_struct.ocidlestate = TIMER_OC_IDLE_STATE_LOW;
    timer_oc_init_struct.ocnidlestate = TIMER_OCN_IDLE_STATE_LOW;

    timer_channel_output_config(M1_MOTOR_TIMER, TIMER_CH_0, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(M1_MOTOR_TIMER, TIMER_CH_0, 1000);
    timer_channel_output_mode_config(M1_MOTOR_TIMER,TIMER_CH_0,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(M1_MOTOR_TIMER,TIMER_CH_0,TIMER_OC_SHADOW_DISABLE);


    timer_channel_output_config(M1_MOTOR_TIMER, TIMER_CH_1, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(M1_MOTOR_TIMER, TIMER_CH_1, 1000);
    timer_channel_output_mode_config(M1_MOTOR_TIMER,TIMER_CH_1,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(M1_MOTOR_TIMER,TIMER_CH_1,TIMER_OC_SHADOW_DISABLE);
    timer_auto_reload_shadow_enable(M1_MOTOR_TIMER);
		
    timer_enable(M1_MOTOR_TIMER);

    rcu_periph_clock_enable(M2_MOTOR_TIMER_CLK);
    timer_deinit(M2_MOTOR_TIMER);
    timer_struct_para_init(&timer_param_type);

    timer_param_type.alignedmode = TIMER_COUNTER_EDGE;
    timer_param_type.clockdivision = TIMER_CKDIV_DIV1;
    timer_param_type.counterdirection = TIMER_COUNTER_UP;
    timer_param_type.period = 1000-1;   //60kHz
    timer_param_type.prescaler = 2-1;   //120Mhz/2=60MHz
    timer_param_type.repetitioncounter = 0;
    timer_init(M2_MOTOR_TIMER, &timer_param_type);

    timer_channel_output_struct_para_init(&timer_oc_init_struct);
    timer_oc_init_struct.ocpolarity = TIMER_OC_POLARITY_HIGH;
    timer_oc_init_struct.outputstate = TIMER_CCX_ENABLE;
    timer_oc_init_struct.outputnstate = TIMER_CCXN_DISABLE;
    timer_oc_init_struct.ocnpolarity = TIMER_OCN_POLARITY_HIGH;
    timer_oc_init_struct.ocidlestate = TIMER_OC_IDLE_STATE_LOW;
    timer_oc_init_struct.ocnidlestate = TIMER_OCN_IDLE_STATE_LOW;

    timer_channel_output_config(M2_MOTOR_TIMER, TIMER_CH_0, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(M2_MOTOR_TIMER, TIMER_CH_0, 1000);
    timer_channel_output_mode_config(M2_MOTOR_TIMER,TIMER_CH_0,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(M2_MOTOR_TIMER,TIMER_CH_0,TIMER_OC_SHADOW_DISABLE);


    timer_channel_output_config(M2_MOTOR_TIMER, TIMER_CH_1, &timer_oc_init_struct);
    timer_channel_output_pulse_value_config(M2_MOTOR_TIMER, TIMER_CH_1, 1000);
    timer_channel_output_mode_config(M2_MOTOR_TIMER,TIMER_CH_1,TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(M2_MOTOR_TIMER,TIMER_CH_1,TIMER_OC_SHADOW_DISABLE);
    timer_auto_reload_shadow_enable(M2_MOTOR_TIMER);

    timer_enable(M2_MOTOR_TIMER);

    return;
}

/**
 -  @brief  电机1的IN1脚PWM脉宽设置
 -  @note   None
 -  @param  pulse:脉宽数值，范围0-1000
 -  @retval None
   */
static void motor1_in1_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(M1_MOTOR_TIMER, TIMER_CH_0, pulse);
}
/**
 -  @brief  电机1的IN2脚PWM脉宽设置
 -  @note   None
 -  @param  pulse:脉宽数值，范围0-1000
 -  @retval None
   */
static void motor1_in2_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(M1_MOTOR_TIMER, TIMER_CH_1, pulse);
}
/**
 -  @brief  电机2的IN1脚PWM脉宽设置
 -  @note   None
 -  @param  pulse:脉宽数值，范围0-1000
 -  @retval None
   */
static void motor2_in1_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(M2_MOTOR_TIMER, TIMER_CH_0, pulse);
}
/**
 -  @brief  电机2的IN2脚PWM脉宽设置
 -  @note   None
 -  @param  pulse:脉宽数值，范围0-1000
 -  @retval None
   */
static void motor2_in2_pwm_pulse_set(uint16_t pulse)
{
    timer_channel_output_pulse_value_config(M2_MOTOR_TIMER, TIMER_CH_1, pulse);
}

/**
 -  @brief  电机1的PWM调节，也就是速度调节
 -  @note   None
 -  @param  value:范围-1000~+1000,正数是正传，负数是反转
 -  @retval None
   */
void motor1_pwm_value_set(int16_t value)
{
    if (value > 1000)
    {
        value = 1000;
    }
    else if (value < -1000)
    {
        value = -1000;
    }
    if (abs(value) <= 50)
    {
        motor1_in1_pwm_pulse_set(1000);
        motor1_in2_pwm_pulse_set(1000);
    }
    else if (value > 50)
    {
        motor1_in1_pwm_pulse_set(1000 - value);
        motor1_in2_pwm_pulse_set(1000);
    }
    else if (value < -50)
    {
        motor1_in1_pwm_pulse_set(1000);
        motor1_in2_pwm_pulse_set(1000 - abs(value));
    }
}

/**
 -  @brief  电机2的PWM调节，也就是速度调节
 -  @note   None
 -  @param  value:范围-1000~+1000,正数是正传，负数是反转
 -  @retval None
   */
void motor2_pwm_value_set(int16_t value)
{
    if (value > 1000)
    {
        value = 1000;
    }
    else if (value < -1000)
    {
        value = -1000;
    }
    if (abs(value) <= 50)
    {
        motor2_in1_pwm_pulse_set(1000);
        motor2_in2_pwm_pulse_set(1000);
    }
    else if (value > 50)
    {
        motor2_in1_pwm_pulse_set(1000);
        motor2_in2_pwm_pulse_set(1000 - value);
    }
    else if (value < -50)
    {
        motor2_in1_pwm_pulse_set(1000 - abs(value));
        motor2_in2_pwm_pulse_set(1000);
    }
}

 /**
  -  @brief  电机初始化
  -  @note   None
  -  @param  None
  -  @retval None
    */
void bsp_motor_init(void)
{
    motor_pwm_gpio_init();
    motor_timer_init();
    return;
}
