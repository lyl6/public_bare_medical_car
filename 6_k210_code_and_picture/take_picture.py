import sensor, image, time, lcd, struct, ustruct
from Maix import GPIO,utils
import gc,os
from machine import Timer,PWM,I2C
from fpioa_manager import fm

lcd.init()                          # Init lcd display
lcd.clear(lcd.RED)                  # Clear lcd screen.

sensor.reset()                      # Reset and initialize the sensor.
sensor.reset(freq=24000000, dual_buff=1)                # 设置摄像头频率 24M 开启双缓冲模式 会提高帧率 但内存占用增加
sensor.set_auto_exposure(1)         # 设置自动曝光
sensor.set_auto_gain(False) # 颜色跟踪必须关闭自动增益
sensor.set_auto_whitebal(False) # 颜色跟踪必须关闭白平衡
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)   # Set frame size to QVGA (320x240)

sensor.set_windowing((224,224)) # 分辨率为B224X224

sensor.set_vflip(1)

sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.

#要拍摄不同的数字就切换这里的数字
need_number_ficture= 1
#保存文件名计数
save_count =0

#注册IO，注意高速GPIO口才有中断
fm.register(35, fm.fpioa.GPIO0)
fm.register(16, fm.fpioa.GPIOHS0)
#构建案件对象
KEY=GPIO(GPIO.GPIOHS0, GPIO.IN, GPIO.PULL_UP)

#按键标志位
key_node = 0
key_press_long = 0

#中断回调函数
def fun(KEY):
    global state,key_node,need_number_ficture
    temp_count = 0

    time.sleep_ms(10) #消除抖动
    while KEY.value()== 0:
        key_node = 1
        time.sleep_ms(10) #长按延时
        #长按检测计数
        temp_count=temp_count+1
    if temp_count >= 50:
        key_node = 0

        beep.duty(50)
        time.sleep_ms(500)
        beep.duty(0)
        time.sleep_ms(100)

        need_number_ficture=need_number_ficture+1
        if(need_number_ficture == 9):
            need_number_ficture=0



#开启中断，下降沿触发
KEY.irq(fun, GPIO.IRQ_FALLING)

#先把文件路径切换到文件卡里面
os.chdir("/sd")
#os.mkdir("img/0")
#os.mkdir("img/1")
#os.mkdir("img/2")
#os.mkdir("img/3")
#os.mkdir("img/4")
#os.mkdir("img/5")
#os.mkdir("img/6")
#os.mkdir("img/7")
#os.mkdir("img/8")




#PWM通过定时器配置，接到IO15引脚
tim = Timer(Timer.TIMER0, Timer.CHANNEL0, mode=Timer.MODE_PWM)
beep = PWM(tim, freq=1000, duty=0, pin=9)


clock = time.clock()                # 创建一个clock对象，用来计算帧率
while True:
    clock.tick()                    # 更新计算帧率的clock
    img=sensor.snapshot()
    #按键按下进入
    if key_node == 1:

        save_count=save_count+1
        img.save("img/"+str(need_number_ficture)+"/"+str(save_count)+".jpg")

        beep.duty(50)
        time.sleep_ms(100)
        beep.duty(0)
        time.sleep_ms(100)

        key_node = 0 #清除按键标志位
    # 在图像上画字符串
    img.draw_string(0, 10,str(need_number_ficture)+"/"+str(save_count)+".jpg", color = (200, 0, 0), scale = 2, mono_space = False,
                    char_rotation = 0, char_hmirror = False, char_vflip = False,
                    string_rotation = 0, string_hmirror = False, string_vflip = False)
    lcd.display(img)

